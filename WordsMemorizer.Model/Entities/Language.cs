﻿namespace WordsMemorizer.Model.Entities
{
    public class Language : IEntity
    {
        public int Id { get; set; }

        public Word Word { get; set; }

        public int WordId { get; set; }
    }
}
