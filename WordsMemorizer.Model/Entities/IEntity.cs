﻿namespace WordsMemorizer.Model.Entities
{
    public interface IEntity
    {
        int Id { get; }
    }
}
