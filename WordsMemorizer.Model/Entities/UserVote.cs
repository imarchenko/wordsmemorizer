﻿namespace WordsMemorizer.Model.Entities
{
    public class UserVote : Vote
    {
        public int UserId { get; set; }

        public UserAccount User { get; set; }

        public Raiting Rank { get; set; }

        public int RankId { get; set; }
    }
}
