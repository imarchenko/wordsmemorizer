﻿using System.Collections.Generic;

namespace WordsMemorizer.Model.Entities
{
    public class UserWord : IEntity
    {
        public int Id { get; private set; }

        public UserLanguage UserLanguage { get; set; }

        public int UserLanguageId { get; set; }

        public Word Word { get; set; }

        public int WordId { get; set; }

        public RepeatSchedule RepeatSchedule { get; set; }

        public int? RepeatScheduleId { get; set; }

        public ICollection<UserTranslation> UserTranslations { get; set; }

        public UserWord()
        {
        }
    }
}