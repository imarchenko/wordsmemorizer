﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities.Identity;
using WordsMemorizer.Model.ValueObjects;

namespace WordsMemorizer.Model.Entities
{
    /// <summary>
    /// Important information about user account
    /// </summary>
    public class UserAccount : IEntity, IEquatable<UserAccount>, IUser<int>
    {
        public UserAccount()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Claims = new List<UserClaim>();
            Logins = new List<UserLogin>();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual UserRoleType UserRole { get; set; }

        /// <summary>
        /// User ID (Primary Key)
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public virtual string UserName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// True if the email is confirmed, default is false
        /// </summary>
        public virtual bool EmailConfirmed { get; set; }

        /// <summary>
        /// The salted/hashed form of the user password
        /// </summary>
        public virtual string PasswordHash { get; set; }

        /// <summary>
        /// DateTime in UTC when lockout ends, any time in the past is considered not locked out.
        /// </summary>
        public virtual DateTime? LockoutEndDateUtc { get; set; }

        /// <summary>
        /// Used to record failures for the purposes of lockout
        /// </summary>
        public virtual int AccessFailedCount { get; set; }

        /// <summary>
        /// Is lockout enabled for this user
        /// </summary>
        public virtual bool LockoutEnabled { get; set; }

        /// <summary>
        /// Navigation property for user claims
        /// </summary>
        public virtual ICollection<UserClaim> Claims { get; private set; }

        /// <summary>
        /// Navigation property for user logins
        /// </summary>
        public virtual ICollection<UserLogin> Logins { get; private set; }

        public virtual UserAccountState State { get; set; }

        public virtual UserProfile Profile { get; set; }

        public virtual int ProfileId { get; set; }

        /// <summary>
        /// A random value that should change whenever a users credentials have changed (password changed, login removed)
        /// </summary>
        public virtual string SecurityStamp { get; set; }

        public bool HasPassword
        {
            get { return !string.IsNullOrWhiteSpace(PasswordHash); }
        }

        public bool Equals(UserAccount other)
        {
            return other.Id == Id;
        }
    }
}