﻿namespace WordsMemorizer.Model.Entities
{
    public class Pronounciation : IEntity
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public Word Word { get; set; }

        public int WordId { get; set; }

        public Raiting Rank { get; set; }

        public int? RankId { get; set; }
    }
}
