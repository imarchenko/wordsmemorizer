﻿using System.Collections.Generic;

namespace WordsMemorizer.Model.Entities
{
    public class Raiting : IEntity
    {
        public int Id { get; set; }

        public decimal Avarage { get; set; }

        public int VotesCount { get; set; }

        public ICollection<UserVote> Voutes { get; set; }
    }
}
