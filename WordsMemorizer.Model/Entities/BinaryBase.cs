﻿namespace WordsMemorizer.Model.Entities
{
    public abstract class BinaryBase : IEntity
    {
        public int Id { get; set; }

        public byte[] Data { get; set; }

        public string MimeType { get; set; }
    }
}
