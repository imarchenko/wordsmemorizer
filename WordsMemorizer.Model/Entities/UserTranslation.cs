﻿namespace WordsMemorizer.Model.Entities
{
    public class UserTranslation : IEntity
    {
        public int Id { get; set; }

        public Translation Translation { get; set; }

        public int TranslationId { get; set; }

        public UserWord UserWord { get; set; }

        public int UserWordId { get; set; }

        public Raiting Raiting { get; set; }

        public int? RaitingId { get; set; }

        public int Popularity { get; set; }
    }
}
