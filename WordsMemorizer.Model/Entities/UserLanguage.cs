﻿using System.Collections.Generic;

namespace WordsMemorizer.Model.Entities
{
    public class UserLanguage : IEntity
    {
        public int Id { get; private set; }

        public Language Language { get; set; }

        public int LanguageId { get; set; }

        public UserAccount UserAccount { get; set; }

        public int UserAccountId { get; set; }

        public List<UserWord> UserWords { get; set; }
    }
}