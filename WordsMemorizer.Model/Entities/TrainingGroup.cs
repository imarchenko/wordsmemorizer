﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsMemorizer.Model.Entities
{
    public class TrainingGroup : IEntity
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public TrainingGroupCategory Category { get; set; }

        public int? RecordId { get; set; }

        public int OrderNumber { get; set; }

        public int UserId { get; set; }

        public UserAccount User { get; set; }
    }
}
