﻿namespace WordsMemorizer.Model.Entities
{
    public class Vote : IEntity
    {
        public int Id { get; set; }

        public int Mark { get; set; }

        public long Date { get; set; }
    }
}
