﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WordsMemorizer.Model.Entities
{
    public class Word
    {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        public Language Language { get; set; }

        public int LagnuageId { get; set; }

        public virtual ICollection<Translation> Translations { get; set; }

        public virtual ICollection<Pronounciation> Pronounciations { get; set; }
    }
}
