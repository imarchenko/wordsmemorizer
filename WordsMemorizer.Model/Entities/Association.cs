﻿namespace WordsMemorizer.Model.Entities
{
    public class Association : IEntity
    {
        public int Id { get; set; }

        public string CombinedAssociation { get; set; }

        public string ForeignWordAssociation { get; set; }

        public string TranslationAssociation { get; set; }

        public int UserId { get; set; }

        public UserAccount User { get; set; }

        public int RankId { get; set; }

        public Raiting Rank { get; set; }

    }
}
