﻿using System;
using System.ComponentModel.DataAnnotations;
using WordsMemorizer.Model.ValueObjects;
using WordsMemorizer.Model.Entities;

namespace WordsMemorizer.Model.Entities
{
    /// <summary>
    /// Additional information about user
    /// </summary>
    public class UserProfile : IEntity
    {
        public int Id { get; set; }

        public string FullName { get; set; }
        
        public Gender Gender { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }

        public int? Age { get; set; }

        public AvatarImage Avatar { get; set; }
              
        [DataType(DataType.Date)]
        public DateTime? RegistrationDate { get; set; }
    }
}
