﻿
namespace WordsMemorizer.Model.Entities.Identity
{
    /// <summary>
    /// EntityType that represents one specific user claim
    /// </summary>
    public class UserClaim : IEntity
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// User Id for the user who owns this claim
        /// </summary>
        public virtual int UserId { get; set; }

        /// <summary>
        /// User who owns this claim
        /// </summary>
        public virtual UserAccount User { get; set; }

        /// <summary>
        /// Claim type
        /// </summary>
        public virtual string ClaimType { get; set; }

        /// <summary>
        /// Claim value
        /// </summary>
        public virtual string ClaimValue { get; set; }
    }
}
