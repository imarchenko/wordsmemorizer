﻿using Microsoft.AspNet.Identity;

namespace WordsMemorizer.Model.Entities.Identity
{
    /// <summary>
    /// Entity type for a user's login (i.e. facebook, google)
    /// </summary>
    public class UserLogin : IEntity
    {
        public virtual int Id { get; protected set; }

        /// <summary>
        /// The login provider for the login (i.e. facebook, google)
        /// </summary>
        public virtual string LoginProvider { get; set; }

        /// <summary>
        /// Key representing the login for the provider
        /// </summary>
        public virtual string ProviderKey { get; set; }

        /// <summary>
        /// User Id for the user who owns this login
        /// </summary>
        public virtual int UserId { get; set; }

        public UserAccount User { get; set; }

        public UserLogin()
        {
        }

        public UserLogin(UserLoginInfo info)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            LoginProvider = info.LoginProvider;
            ProviderKey = info.ProviderKey;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public UserLoginInfo ToUserLoginInfo()
        {
            return new UserLoginInfo(LoginProvider, ProviderKey);
        }
    }
}
