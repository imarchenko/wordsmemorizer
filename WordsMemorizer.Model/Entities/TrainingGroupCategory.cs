﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsMemorizer.Model.Entities
{
    public class TrainingGroupCategory : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int OrderNumber { get; set; }
    }
}
