﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsMemorizer.Model.Entities
{
    public class RepeatSchedule : IEntity
    {
        public const int MinimumHoursToRepeat = 4;
        public const int RepeatProgression = 2;

        public int Id { get; protected set; }

        public DateTime? LastRepeat { get; protected set; }

        public int SuccessedRepeatsCount { get; protected set; }

        public int FailedRepeatsCount { get; protected set; }

        public int HoursToRepeat { get; protected set; }

        public bool NeedToRepeatNow { get; set; }

        public RepeatSchedule()
        {
            this.HoursToRepeat = MinimumHoursToRepeat;
        }

        public void Update(bool isSucceded)
        {
            if (isSucceded)
            {
                this.SuccessedRepeatsCount++;
                this.HoursToRepeat *= RepeatProgression;
            }
            else
            {
                this.FailedRepeatsCount++;
                if (this.HoursToRepeat > MinimumHoursToRepeat)
                {
                    this.HoursToRepeat /= RepeatProgression; 
                }
            }
            this.LastRepeat = DateTime.Now;
        }
    }
}
