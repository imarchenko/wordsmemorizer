﻿namespace WordsMemorizer.Model.Entities
{
    public sealed class ApplicationSettingEntry : IEntity
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}
