﻿namespace WordsMemorizer.Model.Entities
{
    public class AvatarImage : BinaryBase
    {
        public byte[] Thumbnail { get; set; }
    }
}
