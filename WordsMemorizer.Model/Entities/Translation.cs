﻿namespace WordsMemorizer.Model.Entities
{
    public class Translation : IEntity
    {
        public int Id { get; set; }

        public int Word1Id { get; set; }

        public Word Word1 { get; set; }
        
        public int Word2Id { get; set; }

        public Word Word2 { get; set; }
    }
}
