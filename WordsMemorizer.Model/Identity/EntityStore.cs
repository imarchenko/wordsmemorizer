﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace WordsMemorizer.Model.Identity
{
    internal class EntityStore<TEntity> where TEntity : class
    {
        // Methods
        public EntityStore(MainDbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            Context = context;
            DbEntitySet = context.Set<TEntity>();
        }

        // Properties
        public MainDbContext Context { get; private set; }

        public DbSet<TEntity> DbEntitySet { get; private set; }

        public IQueryable<TEntity> EntitySet
        {
            get { return DbEntitySet; }
        }

        public void Create(TEntity entity)
        {
            DbEntitySet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
        }

        public virtual Task<TEntity> GetByIdAsync(object id)
        {
            return DbEntitySet.FindAsync(new[] {id});
        }
    }
}