﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IUserPasswordStore<UserAccount, int>
    {
        /// <summary>
        /// Set the password hash for a user
        /// 
        /// </summary>
        /// <param name="user"/><param name="passwordHash"/>
        /// <returns/>
        public Task SetPasswordHashAsync(UserAccount user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        /// <summary>
        /// Get the password hash for a user
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<string> GetPasswordHashAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            return Task.FromResult(user.PasswordHash);
        }

        /// <summary>
        /// Returns true if the user has a password set
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<bool> HasPasswordAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            return Task.FromResult(user.PasswordHash != null);
        }
    }
}