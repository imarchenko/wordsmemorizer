﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.ValueObjects;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IUserRoleStore<UserAccount, int>
    {
        /// <summary>
        /// Add a user to a role
        /// 
        /// </summary>
        /// <param name="user"/><param name="roleName"/>
        /// <returns/>
        public Task AddToRoleAsync(UserAccount user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(@"ValueCannotBeNullOrEmpty", "roleName");
            }
            ThrowIfDisposed();
            var role = GetRole(roleName);
            user.UserRole = role;
            return Task.FromResult(0);
        }

        /// <summary>
        /// Remove a user from a role
        /// 
        /// </summary>
        /// <param name="user"/><param name="roleName"/>
        /// <returns/>
        public Task RemoveFromRoleAsync(UserAccount user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(@"ValueCannotBeNullOrEmpty", "roleName");
            }
            ThrowIfDisposed();
            var role = GetRole(roleName);
            if (user.UserRole == role)
            {
                user.UserRole = UserRoleType.User;
            }
            return Task.FromResult(0);
        }

        /// <summary>
        /// Get the names of the roles a user is a member of
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<IList<string>> GetRolesAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            IList<string> result = new[] { user.UserRole.ToString() };
            return Task.FromResult(result);
        }

        /// <summary>
        /// Returns true if the user is in the named role
        /// 
        /// </summary>
        /// <param name="user"/><param name="roleName"/>
        /// <returns/>
        public Task<bool> IsInRoleAsync(UserAccount user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(@"ValueCannotBeNullOrEmpty", "roleName");
            }
            ThrowIfDisposed();
            var role = GetRole(roleName);
            return Task.FromResult(user.UserRole == role);
        }
    }
}