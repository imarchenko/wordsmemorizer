﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Entities.Identity;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IUserLoginStore<UserAccount, int>
    {
        /// <summary>
        /// Add a login to the user
        /// 
        /// </summary>
        /// <param name="user"/><param name="login"/>
        /// <returns/>
        public Task AddLoginAsync(UserAccount user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            ThrowIfDisposed();

            var userLogin = new UserLogin();
            var logins = user.Logins;
            userLogin.UserId = user.Id;
            userLogin.ProviderKey = login.ProviderKey;
            userLogin.LoginProvider = login.LoginProvider;

            logins.Add(userLogin);
            return Task.FromResult(0);
        }

        /// <summary>
        /// Remove a login from a user
        /// 
        /// </summary>
        /// <param name="user"/><param name="login"/>
        /// <returns/>
        public Task RemoveLoginAsync(UserAccount user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            ThrowIfDisposed();
            var userLogin = user.Logins
                .SingleOrDefault(l => l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey);
            if (userLogin != null)
            {
                user.Logins.Remove(userLogin);
            }
            return Task.FromResult(0);
        }

        /// <summary>
        /// Get the logins for a user
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<IList<UserLoginInfo>> GetLoginsAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            IList<UserLoginInfo> result = user.Logins.Select(l => new UserLoginInfo(l.LoginProvider, l.ProviderKey))
                .ToList();
            return Task.FromResult(result);
        }

        /// <summary>
        /// Returns the user associated with this login
        /// 
        /// </summary>
        /// <returns/>
        public async Task<UserAccount> FindAsync(UserLoginInfo login)
        {
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            ThrowIfDisposed();
            var userLogin = await _userLogins.Include(u => u.User)
                .FirstOrDefaultAsync(l => l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey);
            return userLogin == null ? null : userLogin.User;
        }
    }
}