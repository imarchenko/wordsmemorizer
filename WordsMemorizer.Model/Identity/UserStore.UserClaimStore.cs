﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Entities.Identity;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IUserClaimStore<UserAccount, int>
    {
        /// <summary>
        /// Return the claims for a user
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<IList<Claim>> GetClaimsAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            IList<Claim> result = user.Claims.Select(c => new Claim(c.ClaimType, c.ClaimValue)).ToList();
            return Task.FromResult(result);
        }

        /// <summary>
        /// Add a claim to a user
        /// 
        /// </summary>
        /// <param name="user"/><param name="claim"/>
        /// <returns/>
        public Task AddClaimAsync(UserAccount user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            ThrowIfDisposed();
            var userClaim = new UserClaim();
            var claims = user.Claims;

            userClaim.UserId = user.Id;
            userClaim.ClaimType = claim.Type;
            userClaim.ClaimValue = claim.Value;
            claims.Add(userClaim);

            return Task.FromResult(0);
        }

        /// <summary>
        /// Remove a claim from a user
        /// 
        /// </summary>
        /// <param name="user"/><param name="claim"/>
        /// <returns/>
        public Task RemoveClaimAsync(UserAccount user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            ThrowIfDisposed();
            foreach (
                var userClaim in user.Claims.Where(uc => uc.ClaimValue == claim.Value && uc.ClaimType == claim.Type))
            {
                user.Claims.Remove(userClaim);
            }

            var userClaims = _userClaims
                .Where(uc => uc.UserId == user.Id && uc.ClaimValue == claim.Value && uc.ClaimType == claim.Type);
            foreach (var userClaim in userClaims)
            {
                _userClaims.Remove(userClaim);
            }
            return Task.FromResult(0);
        }
    }
}