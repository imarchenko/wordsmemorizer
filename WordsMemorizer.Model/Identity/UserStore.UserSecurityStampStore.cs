﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IUserSecurityStampStore<UserAccount, int>
    {
        /// <summary>
        /// Set the security stamp for the user
        /// 
        /// </summary>
        /// <param name="user"/><param name="stamp"/>
        /// <returns/>
        public Task SetSecurityStampAsync(UserAccount user, string stamp)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        /// <summary>
        /// Get the security stamp for a user
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<string> GetSecurityStampAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            return Task.FromResult(user.SecurityStamp);
        }
    }
}