﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Entities.Identity;
using WordsMemorizer.Model.ValueObjects;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IQueryableUserStore<UserAccount, int>
    {
        private IMainDbContext _dbContext;
        private readonly IDbSet<UserAccount> _userAccounts;
        private readonly IDbSet<UserLogin> _userLogins;
        private readonly IDbSet<UserClaim> _userClaims;
        private bool _disposed;

        public UserStore(IMainDbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            _dbContext = dbContext;
            _userAccounts = dbContext.UserAccounts;
            _userLogins = dbContext.UserLogins;
            _userClaims = dbContext.UserClaims;
            DisposeContext = false;
            AutoSaveChanges = true;
        }

        /// <summary>
        /// If true will call dispose on the DbContext during Dipose
        /// 
        /// </summary>
        public bool DisposeContext { get; set; }

        /// <summary>
        /// If true will call SaveChanges after Create/Update/Delete
        /// 
        /// </summary>
        public bool AutoSaveChanges { get; set; }

        private static UserRoleType GetRole(string roleName)
        {
            UserRoleType role;
            if (Enum.TryParse(roleName, out role))
            {
                return role;
            }
            throw new InvalidOperationException(string.Format("role {0} doesn't exists", roleName));
        }

        private async Task SaveChanges()
        {
            if (AutoSaveChanges)
            {
                await _dbContext.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Insert an entity
        /// 
        /// </summary>
        /// <param name="user"/>
        public async Task CreateAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            _userAccounts.Add(user);
            await SaveChanges().ConfigureAwait(false);
        }

        /// <summary>
        /// Update an entity
        /// 
        /// </summary>
        /// <param name="user"/>
        public async Task UpdateAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            _dbContext.MarkAsModified(user);
            await SaveChanges().ConfigureAwait(false);
        }

        /// <summary>
        /// Mark an entity for deletion
        /// 
        /// </summary>
        /// <param name="user"/>
        public async Task DeleteAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            _userAccounts.Remove(user);
            await SaveChanges().ConfigureAwait(false);
        }

        /// <summary>
        /// Find a user by id
        /// 
        /// </summary>
        /// <param name="userId"/>
        /// <returns/>
        public Task<UserAccount> FindByIdAsync(int userId)
        {
            ThrowIfDisposed();
            return _userAccounts
                .Include(i => i.Profile.Avatar)
                .FirstOrDefaultAsync(u=>u.Id == userId);
        }

        /// <summary>
        /// Find a user by name
        /// 
        /// </summary>
        /// <param name="userName"/>
        /// <returns/>
        public Task<UserAccount> FindByNameAsync(string userName)
        {
            ThrowIfDisposed();
            return _userAccounts
                .Include(i => i.Profile.Avatar)
                .FirstOrDefaultAsync(u => u.UserName.ToLower() == userName.ToLower());
        }

        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// If disposing, calls dispose on the Context.  Always nulls out the Context
        /// 
        /// </summary>
        /// <param name="disposing"/>
        protected virtual void Dispose(bool disposing)
        {
            if (DisposeContext && disposing && _dbContext != null)
                _dbContext.Dispose();
            _disposed = true;
            _dbContext = null;
        }

        #endregion IDisposable

        /// <summary>
        /// IQueryable users
        /// 
        /// </summary>
        public IQueryable<UserAccount> Users
        {
            get { return _userAccounts; }
        }
    }
}