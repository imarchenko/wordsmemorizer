﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;

namespace WordsMemorizer.Model.Identity
{
    public partial class UserStore : IUserEmailStore<UserAccount, int>
    {

        /// <summary>
        /// Set the user email
        /// 
        /// </summary>
        /// <param name="user"/><param name="email"/>
        /// <returns/>
        public Task SetEmailAsync(UserAccount user, string email)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            user.Email = email;
            return Task.FromResult(0);
        }

        /// <summary>
        /// Get the user's email
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<string> GetEmailAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            return Task.FromResult(user.Email);
        }

        /// <summary>
        /// Returns whether the user email is confirmed
        /// 
        /// </summary>
        /// <param name="user"/>
        /// <returns/>
        public Task<bool> GetEmailConfirmedAsync(UserAccount user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            return Task.FromResult(user.EmailConfirmed);
        }

        /// <summary>
        /// Sets whether the user email is confirmed
        /// 
        /// </summary>
        /// <param name="user"/><param name="confirmed"/>
        /// <returns/>
        public Task SetEmailConfirmedAsync(UserAccount user, bool confirmed)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            ThrowIfDisposed();
            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        /// <summary>
        /// Find a user by email
        /// 
        /// </summary>
        /// <param name="email"/>
        /// <returns/>
        public Task<UserAccount> FindByEmailAsync(string email)
        {
            ThrowIfDisposed();
            return _userAccounts.FirstOrDefaultAsync(u => u.Email.ToUpper() == email.ToUpper());
        }
    }
}