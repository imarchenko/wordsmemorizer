using WordsMemorizer.Model.SettingEntities;

namespace WordsMemorizer.Model.Service
{
    public interface ISettingService
    {
        T GetSettingEntry<T>() where T : ISettingEntity, new();
        void UpdateSettingEntry<T>(T settingObject) where T : ISettingEntity;
    }
}