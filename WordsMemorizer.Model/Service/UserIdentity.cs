﻿namespace WordsMemorizer.Model.Service
{
    public struct UserIdentity
    {
        private readonly int _userId;
        private readonly string _userName;

        public UserIdentity(int userId, string userName)
        {
            _userId = userId;
            _userName = userName;
        }

        public int UserId
        {
            get { return _userId; }
        }

        public string UserName
        {
            get { return _userName; }
        }
    }
}