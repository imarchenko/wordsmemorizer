﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.SettingEntities;
using WordsMemorizer.Util;

namespace WordsMemorizer.Model.Service
{

    public class EmailService : IIdentityMessageService
    {
        public string SmtpHost { get; protected set; }

        public int SmtpPort { get; protected set; }

        public string SmtpLogin { get; protected set; }

        public string SmtpPassword { get; protected set; }

        //private readonly ISettingService _settingService;

        public EmailService(string smtpHost, int smtpPort, string smtpLogin, string smtpPassword)
        {
            SmtpHost = smtpHost;
            SmtpPort = smtpPort;
            SmtpLogin = smtpLogin;
            SmtpPassword = smtpPassword;
        }

        public async Task SendAsync(IdentityMessage message)
        {
            //var mainSettings = _settingService.GetSettingEntry<MainSettings>();
            var recipients = new List<Tuple<string, Guid>>
            {
                new Tuple<string, Guid>(message.Destination, Guid.Empty)
            };

            await MailBox.SendEmails(SmtpLogin, message.Subject, message.Body, recipients, 
                SmtpLogin, SmtpPassword, SmtpPort, SmtpHost, null);            
        }

        public async Task SendUserAccountDataAsync(string userEmail, int id, string pass)
        {
            //var mainSettings = _settingService.GetSettingEntry<MainSettings>();
            var recipients = new List<Tuple<string, Guid>>
            {
                new Tuple<string, Guid>(userEmail, Guid.Empty)
            };

            var subject = "Регистрация";
            var body = String.Format("Спасибо за регистрацию!<br/>Ваш логин: {0}<br/>Пароль: {1}", id.ToString(), pass);

            await MailBox.SendEmails(SmtpLogin, subject, body, recipients, SmtpLogin, SmtpPassword, SmtpPort, SmtpHost, null); 
        }

        /// <summary>
        /// Check if in the admin main settings enough information to send email
        /// </summary>
        public bool IsAvailable()
        {
            //var mainSettings = _settingService.GetSettingEntry<MainSettings>();
            if (String.IsNullOrEmpty(SmtpLogin) || String.IsNullOrEmpty(SmtpPassword) 
                || String.IsNullOrEmpty(SmtpHost))
            {
                return false;
            }
            return true;
        }

    }
}
