using System;
using System.Data.Entity;
using System.Linq;
using Newtonsoft.Json;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.SettingEntities;

namespace WordsMemorizer.Model.Service
{
    public class SettingService : ISettingService
    {
        private readonly MainDbContext _dbContext;
        private readonly IDbSet<ApplicationSettingEntry> _settingEntries;

        public SettingService(MainDbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            _dbContext = dbContext;
            _settingEntries = _dbContext.ApplicationSettingEntries;
        }

        public virtual T GetSettingEntry<T>() where T : ISettingEntity, new()
        {
            var key = GetSettingEntryKey<T>();
            var settingEntry = _settingEntries.FirstOrDefault(x => x.Key == key);
            if (settingEntry == null)
            {
                return new T();
            }
            var result = JsonConvert.DeserializeObject<T>(settingEntry.Value);
            return result;
        }

        public virtual void UpdateSettingEntry<T>(T settingObject) where T : ISettingEntity
        {
            var key = GetSettingEntryKey<T>();
            var settingEntry = _settingEntries.FirstOrDefault(x => x.Key == key);
            if (settingEntry == null)
            {
                settingEntry = new ApplicationSettingEntry
                                   {
                                       Key = key
                                   };
                _settingEntries.Add(settingEntry);
            }
            var settignObjectSerialized = JsonConvert.SerializeObject(settingObject);
            settingEntry.Value = settignObjectSerialized;
            _dbContext.SaveChanges();
        }

        protected string GetSettingEntryKey<T>()
        {
            var keyAttribute = (SettingKeyAttribute) Attribute.GetCustomAttribute(typeof (T), typeof (SettingKeyAttribute));
            if (keyAttribute == null)
            {
                throw new InvalidOperationException();
            }
            return keyAttribute.Key;
        }
    }
}
