﻿using System.ComponentModel.DataAnnotations;

namespace WordsMemorizer.Model.ValueObjects
{
    public enum UserRoleType
    {
        [Display(Name = "Пользователь")]
        User = 0,
        [Display(Name = "Админ")]
        Admin = 1
    }
}
