﻿namespace WordsMemorizer.Model.ValueObjects
{
    public enum OperationStatus
    {
        None,
        WasNotRun,
        Success,
        Fail,
        Invalid,
        Error,
        Canceled,
        InProgress
    }
}
