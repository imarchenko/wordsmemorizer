﻿namespace WordsMemorizer.Model.ValueObjects
{
    public enum UserAccountState
    {
        Normal = 0,
        Blocked = 1,
        Removed = 2
    }
}
