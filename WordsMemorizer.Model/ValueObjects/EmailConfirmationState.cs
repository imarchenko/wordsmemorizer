﻿namespace WordsMemorizer.Model.ValueObjects
{
    public enum EmailConfirmationState
    {
        NotSent,
        Sent,
        Confirmed,
        NotExist
    }
}
