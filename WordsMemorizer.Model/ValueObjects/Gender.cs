﻿using System.ComponentModel.DataAnnotations;

namespace WordsMemorizer.Model.ValueObjects
{
    public enum Gender
    {
        [Display(Name = "Не выбрано")]
        Unknown,
        [Display(Name = "Мужчина")]
        Male,
        [Display(Name = "Женщина")]
        Female
    }

    //public static class Genders
    //{
    //    static Genders()
    //    {
    //        List = new List<Gender> { Gender.Unknown, Gender.Male, Gender.Female };
    //        SelectCollectionItem = new List<SelectCollectionItem>
    //        {
    //            new SelectCollectionItem
    //            {
    //                Selected = true,
    //                Text = "Не выбрано",
    //                Value = Gender.Unknown.ToString()
    //            },
    //            new SelectCollectionItem
    //            {
    //                Text = "Мужской",
    //                Value = Gender.Male.ToString()
    //            },
    //            new SelectCollectionItem
    //            {
    //                Text = "Женский",
    //                Value = Gender.Female.ToString()
    //            }
    //        };
            
    //    }

    //    public static IEnumerable<Gender> List { get; private set; }

    //    public static IEnumerable<SelectCollectionItem> SelectCollectionItem { get; private set; }

    //    public static string GetGenderText(Gender gender)
    //    {
    //        var genderItem = SelectCollectionItem.FirstOrDefault(g => g.Value == gender.ToString());
    //        return genderItem == null ? string.Empty : genderItem.Text;
    //    }
    //}
}