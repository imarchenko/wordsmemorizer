﻿namespace WordsMemorizer.Model.ValueObjects
{
    public class Size
    {
        public int Width { get; set; }

        public int Height { get; set; }
    }
}
