﻿using System.Data.Entity;
using System.Threading.Tasks;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Entities.Identity;

namespace WordsMemorizer.Model
{
    public interface IMainDbContext
    {
        IDbSet<UserAccount> UserAccounts { get; }

        IDbSet<UserLogin> UserLogins { get; set; }

        IDbSet<ApplicationSettingEntry> ApplicationSettingEntries { get; }

        IDbSet<UserClaim> UserClaims { get; set; }

        IDbSet<UserLanguage> DictionaryLanguages { get; set; }

        IDbSet<UserWord> UserWords { get; set; }

        IDbSet<RepeatSchedule> RepeatSchedules { get; set; }

        IDbSet<TrainingGroup> TrainingGroups { get; set; }

        IDbSet<TrainingGroupCategory> TrainingGroupCategories { get; set; }

        IDbSet<Word> Words { get; set; }

        IDbSet<Pronounciation> WordPronunciations { get; set; }

        IDbSet<Translation> WordTranslations { get; set; }

        void Dispose();

        void MarkAsModified(object entity);

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}