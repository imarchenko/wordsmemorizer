﻿using System;

namespace WordsMemorizer.Model
{
    public struct Paging
    {
        private readonly int _pageNumber;
        private readonly int _pageSize;

        public Paging(int pageNumber, int pageSize)
        {
            if (pageNumber < 1)
            {
                throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "Page number must be greater than zero");
            }
            if (pageSize < 1)
            {
                throw new ArgumentOutOfRangeException("pageSize", pageSize, "Page size must be greater than zero");
            }
            _pageNumber = pageNumber;
            _pageSize = pageSize;
        }

        public int PageNumber
        {
            get { return _pageNumber; }
        }

        public int PageSize
        {
            get { return _pageSize; }
        }
    }
}
