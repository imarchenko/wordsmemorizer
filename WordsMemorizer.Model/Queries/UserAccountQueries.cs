﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.ValueObjects;

namespace WordsMemorizer.Model.Queries
{
    public static class UserAccountQueries
    {
        public static Expression<Func<UserAccount, bool>> UserAccountId(int id)
        {
            return u => u.Id == id;
        }
        
        public static Expression<Func<UserAccount, bool>> SameUserName(string userName)
        {
            userName = userName.ToLower();
            return u => u.UserName.ToLower().Contains(userName);
        }

        public static Expression<Func<UserAccount, bool>> ExtendedSearch(string login, Gender gender, string classRoom, int ageFrom, int ageTill)
        {
            return u => (u.UserName.Contains(login) || string.IsNullOrEmpty(login)) &&
                            (u.Profile.Gender == gender || gender == Gender.Unknown) &&
                            (u.Profile.Age >= ageFrom || ageFrom == 0) &&
                            (u.Profile.Age <= ageTill || ageTill == 0);
        }

        public static Task<bool> AnyAdminExists(this IDbSet<UserAccount> userAccounts)
        {
            return userAccounts.AnyAsync(u => u.UserRole == UserRoleType.Admin);
        }        
        
        
        public static Task<UserAccount> FindUserAccountAsync(this IDbSet<UserAccount> userAccounts, 
            int id,
            params Expression<Func<UserAccount, object>>[] include)
        {
            var query = include.Aggregate(userAccounts.AsQueryable(), (current, path) => current.Include(path));
            return query.FirstOrDefaultAsync(u => u.Id == id);
        }
        
        public static Task<UserAccount> FindUserAccountAsync(this IDbSet<UserAccount> userAccounts, 
            string userName,
            params Expression<Func<UserAccount, object>>[] include)
        {
            var query = include.Aggregate(userAccounts.AsQueryable(), (current, path) => current.Include(path));
            return query.FirstOrDefaultAsync(u => u.UserName == userName);
        }

        public static Task<IQueryable<UserAccount>> FindUserByRoleAsync(this IDbSet<UserAccount> userAccounts,
            UserRoleType userRole, params Expression<Func<UserAccount, object>>[] include)
        {
            var query = include.Aggregate(userAccounts.AsQueryable(), (current, path) => current.Include(path));
            return Task.FromResult(query.Where(u => u.UserRole == userRole));
        }
                 
    }
}