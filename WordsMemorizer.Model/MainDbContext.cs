using System.Data.Entity;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Entities.Identity;

namespace WordsMemorizer.Model
{
    public class MainDbContext : DbContext, IMainDbContext
    {
        public MainDbContext()
            : base("DefaultConnection")
        {
        }

        public IDbSet<UserAccount> UserAccounts { get; set; }

        public IDbSet<UserLogin> UserLogins { get; set; }

        public IDbSet<ApplicationSettingEntry> ApplicationSettingEntries { get; set; }

        public IDbSet<UserClaim> UserClaims { get; set; }

        public IDbSet<UserLanguage> DictionaryLanguages { get; set; }

        public IDbSet<UserWord> UserWords { get; set; }

        public IDbSet<RepeatSchedule> RepeatSchedules { get; set; }

        public IDbSet<TrainingGroup> TrainingGroups { get; set; }

        public IDbSet<TrainingGroupCategory> TrainingGroupCategories { get; set; }

        public IDbSet<Word> Words { get; set; }

        public IDbSet<Pronounciation> WordPronunciations { get; set; }

        public IDbSet<Translation> WordTranslations { get; set; }

        public void MarkAsModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new DataTypeDateConvention());

            modelBuilder.Entity<UserLogin>().HasKey(l => new { l.LoginProvider, l.ProviderKey, l.UserId });

            //modelBuilder.Entity<Word>().HasMany(s => s.Translations).WithMany(c => c.Translations).Map(cs => cs.ToTable("ForeignWordsTranslations"));

            modelBuilder.Configurations.AddFromAssembly(GetType().Assembly);
        }
    }
}