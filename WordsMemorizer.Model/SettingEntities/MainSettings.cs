﻿using System.Collections.Generic;
using WordsMemorizer.Model.ValueObjects;
using System.Linq;

namespace WordsMemorizer.Model.SettingEntities
{
    [SettingKey("MainSettings")]
    public class MainSettings : ISettingEntity
    {
        public string EmailSupport { get; set; }
    }
}
