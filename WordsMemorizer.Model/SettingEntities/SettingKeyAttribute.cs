﻿using System;

namespace WordsMemorizer.Model.SettingEntities
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class SettingKeyAttribute : Attribute
    {
        private readonly string _key;

        public SettingKeyAttribute(string key)
        {
            _key = key;
        }

        public string Key
        {
            get { return _key; }
        }
    }
}
