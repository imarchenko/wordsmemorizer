﻿using WordsMemorizer.Model.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace WordsMemorizer.Model.Mappings
{
    public class UserAccountMapping : EntityTypeConfiguration<UserAccount>
    {
        public UserAccountMapping()
        {
            var indexAttribute = new IndexAttribute("UserNameIndex") {IsUnique = true};
            var indexAnnotation = new IndexAnnotation(indexAttribute);
            Property(u => u.UserName).HasColumnAnnotation("Index", indexAnnotation);

            Property(u => u.UserName).IsRequired().HasMaxLength(256);
            Property(u => u.Email).HasMaxLength(256);

            HasMany(u => u.Claims).WithRequired().HasForeignKey(c => c.UserId);
            HasMany(u => u.Logins).WithRequired().HasForeignKey(c => c.UserId);
            

        }
         
    }
}