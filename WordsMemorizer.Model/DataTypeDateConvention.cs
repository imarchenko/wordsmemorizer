﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace WordsMemorizer.Model
{
    public class DataTypeDateConvention : Convention
    {
        public DataTypeDateConvention()
        {
            Properties<DateTime>()
                .Where(x => x.GetCustomAttributes(false)
                    .OfType<DataTypeAttribute>()
                    .Any(a => a.DataType == DataType.Date))
                .Configure(c => c.HasColumnType("date"));
        }
    }
}