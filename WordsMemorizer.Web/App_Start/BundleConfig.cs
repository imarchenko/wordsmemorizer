﻿using System.Web.Optimization;

namespace WordsMemorizer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            RenderScripts(bundles);
            RenderStyles(bundles);

            // Set EnableOptimizations to false for debugging. For more information, visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }

        private static void RenderScripts(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/extensions").Include(
                        "~/Scripts/Extensions/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryform").Include(
                        "~/Scripts/jquery.form.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when
            // you're ready for production, use the build tool at http://modernizr.com to pick only
            // the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                      "~/Scripts/knockout-{version}.js",
                      "~/Scripts/knockout.mapping-latest.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajaxFormHelper").Include(
                      "~/Scripts/Helpers/ajaxFormHelper.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockoutHelper").Include(
                      "~/Scripts/Helpers/knockoutHelper.js"));

            bundles.Add(new ScriptBundle("~/bundles/progressHelper").Include(
                      "~/Scripts/Helpers/progressHelper.js"));

            bundles.Add(new ScriptBundle("~/bundles/repeatTimeHelper").Include(
                      "~/Scripts/Helpers/repeatTimeHelper.js"));

            bundles.Add(new ScriptBundle("~/bundles/wordsConstructorViewModel").Include(
                      "~/Scripts/ViewModels/wordsConstructorViewModel.js"));

            bundles.Add(new ScriptBundle("~/bundles/viewWordsViewModel").Include(
                      "~/Scripts/ViewModels/viewWordsViewModel.js"));
        }

        private static void RenderStyles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/Pages/Admin/Shared/css").Include(
                      "~/Content/Pages/Admin/Shared/AdminLayout.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/jqueryui").Include(
                      "~/Content/themes/base/dialog.css",
                      "~/Content/themes/base/theme.css",
                      "~/Content/themes/base/core.css"));
        }
    }
}