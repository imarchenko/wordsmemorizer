﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WordsMemorizer.Web
{
    public partial class Startup
    {
        public void ConfigureJsonSerializer()
        {
            JsonNetResult.DefaultSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }
         
    }
}