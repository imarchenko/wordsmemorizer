﻿using WordsMemorizer.Web.Filter;
using System.Web;
using System.Web.Mvc;

namespace WordsMemorizer.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
