﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Autofac;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Owin;
using WordsMemorizer.Model;
using WordsMemorizer.Web.Extensions;

namespace WordsMemorizer.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app, IContainer container)
        {
            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<MainDbContext>());
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManagerExtensions.CreateApplicationUserManager);

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });
            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);


        }
    }
}