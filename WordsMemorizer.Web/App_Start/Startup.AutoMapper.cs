﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WordsMemorizer.Util.Extensions;

namespace WordsMemorizer.Web
{
    public partial class Startup
    {
        public void ConfigureAutoMapper()
        {
            Mapper.Initialize(m => GetProfiles().ForEach(t => m.AddProfile((Profile)Activator.CreateInstance(t))));
            //Mapper.AssertConfigurationIsValid();
        }

        private static IEnumerable<Type> GetProfiles()
        {
            var profiles = typeof(Startup)
                .Assembly.GetTypes()
                .Where(type => !type.IsAbstract && typeof(Profile).IsAssignableFrom(type));
            return profiles;
        }
    }
}