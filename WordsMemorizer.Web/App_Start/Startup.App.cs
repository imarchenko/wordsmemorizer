﻿using Owin;

namespace WordsMemorizer.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureApp(IAppBuilder app)
        {
            var container = ConfigureAutofac();

            ConfigureAuth(app, container);
            
            ConfigureAutoMapper();
            ConfigureDatabase();
            ConfigureJsonSerializer();
        }
    }
}