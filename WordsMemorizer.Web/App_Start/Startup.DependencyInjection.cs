﻿using System;
using WordsMemorizer.Model;
// ReSharper disable once RedundantUsingDirective
using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Util;


namespace WordsMemorizer.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public IContainer ConfigureAutofac()
        {
            var builder = new ContainerBuilder();

            var executingAssembly = Assembly.GetExecutingAssembly();
            var assemblies = new[]
                             {
                                 executingAssembly,
                                 Assembly.GetAssembly(typeof (IEntity)),
                                 Assembly.GetAssembly(typeof (MainDbContext))
                             };


            //RegistrationExtensions.RegisterHubs(builder, executingAssembly);
            builder.RegisterModelBinders(executingAssembly);
            builder.RegisterControllers(executingAssembly);
            builder.RegisterModule(new AutofacWebTypesModule());


            builder.RegisterAssemblyTypes(assemblies)
                .AsImplementedInterfaces()
                .AsSelf();

            //builder.RegisterInstance(AppSettings.Current);

            builder.RegisterType<MainDbContext>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerHttpRequest();
            
            
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            return container;
        }
    }
}