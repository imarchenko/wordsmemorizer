﻿using WordsMemorizer.Model.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsMemorizer.Areas.Admin.ViewModels.Users
{
    public class AdminSetRoleForm
    {
        public UserRoleType UserRoleType { get; set; }

        public int UserId { get; set; }
    }
}