﻿using WordsMemorizer.Model.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsMemorizer.Areas.Admin.ViewModels.Users
{
    public class AdminUsersViewModel
    {
        public AdminSetRoleForm Form { get; set; }

        public List<User> Users { get; set; }

        public class User
        {
            public int Id { get; set; }

            public string FullName { get; set; }

            public string UserName { get; set; }

            public string Email { get; set; }

            public string Phone { get; set; }

            public DateTime RegistrationDate { get; set; }
        
            public UserRoleType UserRole { get; set; }

            public UserAccountState State { get; set; }
        }
    }
}