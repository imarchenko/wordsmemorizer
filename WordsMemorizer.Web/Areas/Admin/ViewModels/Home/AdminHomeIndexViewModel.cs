﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsMemorizer.Areas.Admin.ViewModels.Home
{
    public class AdminHomeIndexViewModel
    {
        public AdminHomeIndexForm Form { get; set; }

        public string UserName { get; set; }

        public int UserId { get; set; }
    }
}