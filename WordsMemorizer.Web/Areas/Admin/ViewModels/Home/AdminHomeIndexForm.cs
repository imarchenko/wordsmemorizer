﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordsMemorizer.Areas.Admin.ViewModels.Home
{
    public class AdminHomeIndexForm
    {
        public bool BecomeAdmin { get; set; }
    }
}