﻿using WordsMemorizer.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WordsMemorizer.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return AreaNames.Admin;
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            var adminUrl = ConfigurationManager.AppSettings["SecretWord"];

            context.MapRoute(
                "Admin_default",
                adminUrl + "/{controller}/{action}/{id}",
                new { controller = "AdminHome", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}