﻿using WordsMemorizer.Areas.Admin.ViewModels.Users;
using WordsMemorizer.Model;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.ValueObjects;
using WordsMemorizer.Web;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using AutoMapper;
using WordsMemorizer.Util;
using WordsMemorizer.Web.Urls;
using System.Transactions;

namespace WordsMemorizer.Areas.Admin.Controllers
{
    [Authorize(Roles = UserRoles.RolesWithAccessToAdminPanel)]
    public class AdminUsersController : Controller
    {
        private readonly MainDbContext _dbContext;
        private readonly IDbSet<UserAccount> _userAccounts;

        public AdminUsersController(MainDbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            _dbContext = dbContext;
            _userAccounts = _dbContext.UserAccounts;
        }


        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
        }

        //
        // GET: /Admin/AdminUsers/
        public ActionResult Index()
        {
            return Redirect(Url.AdminUsersShow());
        }


        public async Task<ActionResult> Show()
        {
            var model = new AdminUsersViewModel();
            var users = await _userAccounts.Include(a => a.Profile).ToListAsync();

            Mapper.Map(users, model);

            foreach (var user in model.Users)
            {
                var roles = await UserManager.GetRolesAsync(user.Id);
                UserRoleType role;
                Enum.TryParse(roles.FirstOrDefault(), out role);
                user.UserRole = role;
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SetRole(AdminSetRoleForm form)
        {
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            var targetUser = await UserManager.FindByIdAsync(form.UserId);
            if (currentUser.UserRole == UserRoleType.User)
            {
                return Redirect(Url.AdminUsersShow());
            }
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var identityResult = await UserManager.SetRoleAsync(form.UserId, form.UserRoleType.ToString());
                if (identityResult.Succeeded)
                {
                    _dbContext.SaveChanges();
                }
                scope.Complete();
            }

            return Redirect(Url.AdminUsersShow());
        }

        [HttpPost]
        public async Task<ActionResult> Ban(int userId)
        {
            try
            {
                await ChangeUserAccountState(userId, UserAccountState.Blocked);
                return new JsonNetResult
                {
                    Data = true
                };
            }
            catch (Exception)
            {
                return new JsonNetResult
                {
                    Data = false
                };
            }
        }

        [HttpPost]
        public async Task<ActionResult> Unban(int userId)
        {
            try
            {
                await ChangeUserAccountState(userId, UserAccountState.Normal);
                return new JsonNetResult
                {
                    Data = true
                };
            }
            catch (Exception)
            {
                return new JsonNetResult
                {
                    Data = false
                };
            }
        }

        private async Task ChangeUserAccountState(int userId, UserAccountState state)
        {
            var user = await UserManager.FindByIdAsync(userId);
            user.State = state;
            await UserManager.UpdateAsync(user);
            _dbContext.SaveChanges();
        }
    }
}