﻿using WordsMemorizer.Model;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Util;
using WordsMemorizer.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WordsMemorizer.Web.Urls;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Queries;
using WordsMemorizer.Areas.Admin.ViewModels.Home;

namespace WordsMemorizer.Areas.Admin.Controllers
{
    [Authorize(Roles = UserRoles.RolesWithAccessToAdminPanel)]
    public class AdminHomeController : Controller
    {
        private readonly IMainDbContext _dbContext;
        private readonly ISettingService _settingsService;

        public AdminHomeController(IMainDbContext dbContext,
            ISettingService settingsService)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            if (settingsService == null)
            {
                throw new ArgumentNullException("settingsService");
            }
            _dbContext = dbContext;
            _settingsService = settingsService;
        }


        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            var isAdmin = User.IsInRole(UserRoles.Admin);

            if (!User.Identity.IsAuthenticated)
            {
                return Redirect(Url.AccountLogin(Url.AdminHomeMain()));
            }

            if (isAdmin)
            {
                return Redirect(Url.AdminHomeMain());
            }

            var anyAdminExist = await AnyAdminExist();
            if (anyAdminExist)
            {
                return Redirect(Url.AccountLogin(Url.AdminHomeMain()));
            }
            
            var model = new AdminHomeIndexViewModel
            {
                UserId = User.Identity.GetUserId<int>(),
                UserName = User.Identity.Name
            };
            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Index(AdminHomeIndexForm form)
        {
            if (form.BecomeAdmin && !(await AnyAdminExist()) && User.Identity.IsAuthenticated)
            {
                var result = await UserManager.SetRoleAsync(User.Identity.GetUserId<int>(), UserRoles.Admin);
                if (result.Succeeded)
                {
                    await _dbContext.SaveChangesAsync();
                    return Redirect(Url.AccountRelogin(Url.AdminHomeMain()));
                }
            }
            return Redirect(Url.HomeIndex());
        }

        private async Task<bool> AnyAdminExist()
        {
            return await _dbContext.UserAccounts.AnyAdminExists();
        }

        public async Task<ActionResult> Main()
        {
            var model = new AdminHomeMainViewModel();
            return View(model);
        }


    }
}