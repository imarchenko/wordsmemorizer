﻿using WordsMemorizer.Web.ViewModels.Api.Training;

namespace WordsMemorizer.Web.ViewModels.Training
{
    public class TrainingWordsConstructorViewModel
    {
        public int GroupId { get; set; }
    }
}