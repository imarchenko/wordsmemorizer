﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WordsMemorizer.Web.ViewModels.Training
{
    public class TrainingIndexViewModel
    {
        public List<SelectListItem> TrainingGroups { get; set; }

        public TrainingIndexViewModel()
        {
            TrainingGroups = new List<SelectListItem>();
        }

    }
}