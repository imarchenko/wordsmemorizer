﻿namespace WordsMemorizer.Web.ViewModels.Api.Training
{
    public class TrainingApiTranslationViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int Popularity { get; set; }

        public decimal Raiting { get; set; }

        public int VotesCount { get; set; }

        public bool HasMyVote { get; set; }
    }
}