﻿using System.Collections.Generic;

namespace WordsMemorizer.Web.ViewModels.Api.Training
{
    public class TrainingApiWordViewModel
    {
        public int WordId { get; set; }

        public string WordText { get; set; }

        public IEnumerable<TrainingApiTranslationViewModel> Translations { get; set; }
    }
}