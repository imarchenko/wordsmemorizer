﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsMemorizer.Web.ViewModels.Api.Dictionary
{
    public class DictionaryApiLanguageViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int WordsCount { get; set; }
    }
}