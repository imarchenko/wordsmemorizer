﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WordsMemorizer.Web.ViewModels.Api.Dictionary
{
    public class DictionaryApiAddLanguageForm
    {
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
    }
}