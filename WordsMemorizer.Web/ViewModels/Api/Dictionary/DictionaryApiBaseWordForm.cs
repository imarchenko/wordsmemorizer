﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WordsMemorizer.Web.ViewModels.Api.Dictionary
{
    public class DictionaryApiBaseWordForm
    {
        public int? WordId { get; set; }

        public int? ForeignWordId { get; set; }
        
        [Display(Name = "Слово")]
        public string Text { get; set; }

        public int? TranscriptionId { get; set; }

        [Display(Name = "Произношение")]
        public string Transcription { get; set; }

        public int? TranslationId { get; set; }

        [Display(Name = "Перевод")]
        public string Translation { get; set; }

        [Display(Name = "Ассоциация слова")]
        public string WordAssociation { get; set; }

        [Display(Name = "Ассоциация перевода")]
        public string TranslationAssociation { get; set; }

        [Display(Name = "Комбинированная ассоциация")]
        public string CombineAssociation { get; set; }
    }
}