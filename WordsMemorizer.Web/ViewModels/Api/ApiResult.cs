﻿namespace WordsMemorizer.Web.ViewModels.Api
{
    public class ApiResult
    {
        public const string GeneralErrorMessage = "Unknown error";

        public object Data { get; set; }

        public string Error { get; set; }
    }
}