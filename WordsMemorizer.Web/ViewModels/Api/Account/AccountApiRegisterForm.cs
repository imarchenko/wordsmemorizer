﻿using System.ComponentModel.DataAnnotations;

namespace WordsMemorizer.Web.ViewModels.Api.Account
{
    public class AccountApiRegisterForm
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Логин")]
        [MinLength(4)]
        public string UserName { get; set; }
    }
}