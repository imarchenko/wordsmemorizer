﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsMemorizer.Web.ViewModels.Api.Account
{
    public class AccountApiLoginForm
    {
        public AccountApiLoginForm()
        {
        }

        [Required]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        [Required]
        [Display(Name = "Логин")]
        public string UserName { get; set; }
    }
}