﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsMemorizer.Web.ViewModels.Api.Dictionary;

namespace WordsMemorizer.Web.ViewModels.Dictionary
{
    public class DictionaryEditWordViewModel
    {
        public int LanguageId { get; set; }

        public DictionaryApiEditWordForm EditWordForm { get; set; }

        public DictionaryEditWordViewModel()
        {
            this.EditWordForm = new DictionaryApiEditWordForm();
        }
    }
}