﻿using WordsMemorizer.Web.ViewModels.Api.Dictionary;

namespace WordsMemorizer.Web.ViewModels.Dictionary
{
    public class DictionaryAddWordViewModel
    {
        public DictionaryApiAddWordForm AddWordForm { get; set; }

        public DictionaryAddWordViewModel()
        {
            this.AddWordForm = new DictionaryApiAddWordForm();
        }
    }
}