﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsMemorizer.Web.ViewModels.Api.Dictionary;

namespace WordsMemorizer.Web.ViewModels.Dictionary
{
    public class DictionaryAddLanguageViewModel
    {
        public DictionaryAddLanguageViewModel()
        {
            this.AddLanguageForm = new DictionaryApiAddLanguageForm();
        }

        public DictionaryApiAddLanguageForm AddLanguageForm { get; set; }
    }
}