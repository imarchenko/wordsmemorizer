﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordsMemorizer.Web.ViewModels.Api.Dictionary;

namespace WordsMemorizer.Web.ViewModels.Dictionary
{
    public class DictionaryEditLanguageViewModel
    {
        public DictionaryEditLanguageViewModel()
        {
            this.EditLanguageForm = new DictionaryApiEditLanguageForm();
        }

        public DictionaryApiEditLanguageForm EditLanguageForm { get; set; }
    }
}