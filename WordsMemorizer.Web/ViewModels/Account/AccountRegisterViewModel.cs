﻿using WordsMemorizer.Web.ViewModels.Api.Account;

namespace WordsMemorizer.ViewModels.Account
{
    public class AccountRegisterViewModel
    {
        public AccountRegisterViewModel()
        {
            RegisterForm = new AccountApiRegisterForm();
        }

        public AccountApiRegisterForm RegisterForm { get; set; }
    }
}