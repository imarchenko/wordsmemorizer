﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordsMemorizer.Web.ViewModels.Api.Account;

namespace WordsMemorizer.ViewModels.Account
{
    public class AccountLoginViewModel
    {
        public AccountLoginViewModel()
        {
            LoginForm = new AccountApiLoginForm();
        }

        public AccountApiLoginForm LoginForm { get; set; }
    }
}