﻿using System.Data.Entity;
using System.Linq;
using WordsMemorizer.Model;

namespace WordsMemorizer.Web.CronConfig
{
    public static class CronJobs
    {
        private static IMainDbContext _dbContext;

        static CronJobs()
        {
            _dbContext = new MainDbContext();
        }

        public static void WordIsReadyForTraining(int wordId)
        {
            var word = _dbContext.UserWords
                .Include(i => i.RepeatSchedule)
                .FirstOrDefault(w => w.Id == wordId);

            word.RepeatSchedule.NeedToRepeatNow = true;

            _dbContext.SaveChanges();
        }
    }
}