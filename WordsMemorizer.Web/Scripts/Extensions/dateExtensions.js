﻿Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
}

Date.prototype.getDiffInMin = function (date) {
    var diff = this - date;
    if (diff < 0)
    {
        return 0;
    }
    return diff / 1000 / 60;
}