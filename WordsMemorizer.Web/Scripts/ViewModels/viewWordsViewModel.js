﻿var viewWordsViewModel = function(){
    var self = this;

    self.anyReadyWords = function (words) {
        var any = false;
        if (words != undefined) {
            words.forEach(function (word) {
                if (word.repeatSchedule.needToRepeatNow) {
                    any = true;
                    return true;
                }
            });
        }
        return any;
    }
};