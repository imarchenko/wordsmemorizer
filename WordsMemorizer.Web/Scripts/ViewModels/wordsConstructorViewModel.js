﻿var wordsConstructorViewModel = function(){
    var self = this;

    function gameInfo() {
        var game = this;
        game.koCurrentStep = ko.observable(0),
        game.currentWord = {
            koId: ko.observable(0),
            koLetters: ko.observableArray(),
            koTranslation: ko.observable('')
        };
        game.koCurrentWords = ko.observableArray();
        game.koAllWords = ko.observableArray();
        game.koWordsNeedRepeat = ko.observableArray();
        game.koIsStepFinished = ko.observable(false);
        game.koIsRepeatingStageNow = ko.observable(false);
        game.koIsGameFinished = ko.observable(false);
        //game.koIsThisTheLastWord = ko.computed(function () {
        //    var wordsLeft = game.koCurrentWords().length;
        //    var wordsNeedRepeatLeft = game.koWordsNeedRepeat().length;
        //    var isItRepeating = game.koIsRepeatingStageNow();
        //    console.log(wordsLeft);
        //    if (wordsLeft == 1 && ((isItRepeating) || (wordsNeedRepeatLeft == 1))) {
        //        return true;
        //    }
        //    return false;
        //});

        return game;
    };

    self.game = gameInfo();
    self.addCurrentWordToRepeatList = function () {
        var currentWord = self.game.currentWord;
        var word = self.getWordById(currentWord.koId());
        var shuffleWord = self.shuffleWord(word);
        self.game.koWordsNeedRepeat.push(shuffleWord);
    };
    self.startGame = function (words) {
        self.game.koAllWords(words);
        var shuffledWords = self.shuffleWords(words);
        self.game.koCurrentWords(shuffledWords);
        if (shuffledWords.length > 0) {
            var firstWord = shuffledWords[0];
            self.setCurrentWord(firstWord.wordId, firstWord.translation);
        }
    };
    self.getWordById = function (id) {
        var words = self.game.koAllWords();
        for (var i = 0; i < words.length; i++) {
            if (words[i].wordId == id) {
                return words[i];
            }
        }
    };
    self.setCurrentWord = function(id, translation){
        self.game.currentWord.koId(id);
        self.game.currentWord.koTranslation(translation);
    };
    self.shuffleLetters = function (letters) {
        var shuffledLetters = [];
        for (var i = letters.length; i > 0 ; i--) {
            var randIndex = Math.floor((Math.random() * i));
            var letter = letters[randIndex];

            shuffledLetters.push(letter);
            letters.splice(randIndex, 1);
        }
        return shuffledLetters;
    };
    self.shuffleWords = function (words) {
        var shuffledWords = [];
        for (var i = 0; i < words.length; i++) {
            var word = words[i];
            shuffledWords.push(self.shuffleWord(word));
        }
        return shuffledWords;
    };
    self.shuffleWord = function (word) {
        var letters = word.wordText.toUpperCase().split('');

        var shuffleWord = {
            wordId: word.wordId,
            koShuffledLetters: ko.observableArray(self.shuffleLetters(letters.slice())),
            wordLetters: letters,
            translation: word.translation
        };
        return shuffleWord;
    };
    self.nextWord = function () {
        if (self.hasMoreWords(false)) {
            self.resetCurrentWord();

            var step = self.game.koCurrentStep();
            step++;
            self.game.koCurrentStep(step);
            var nextWord = self.game.koCurrentWords()[step];
            self.setCurrentWord(nextWord.wordId, nextWord.translation);

            self.game.koIsStepFinished(false);
        }
        else if (self.game.koWordsNeedRepeat().length > 0 &&
                !self.game.koIsRepeatingStageNow()) {
            self.repeatWords();

            self.game.koIsStepFinished(false);
        }
        else {
            wordsMemorizer_ajaxFormHelper.manuallySubmit($(window.saveTraining), function () {
                self.game.koIsGameFinished(true);
                self.game.koIsStepFinished(false);
            });
        }
    };
    self.isStepVisible = function (step) {
        var currentStep = self.game.koCurrentStep();
        return currentStep == step;
    };
    self.checkLetter = function (letter) {
        var checkedLetters = self.game.currentWord.koLetters();
        var gameWords = self.game.koCurrentWords();
        var currentStep = self.game.koCurrentStep();
        var correctWord = gameWords[currentStep].wordLetters;

        if (checkedLetters.length < correctWord.length) {
            var isLetterCorect = correctWord[checkedLetters.length] == letter;
            if (!isLetterCorect) {
                var wordsOnRepeat = self.game.koWordsNeedRepeat();
                var currentWordId = self.game.currentWord.koId();
                var alreadyInTheList = false;
                for (var i = 0; i < wordsOnRepeat.length; i++) {
                    if (wordsOnRepeat[i].wordId == currentWordId) {
                        alreadyInTheList = true;
                    }
                }
                if (!alreadyInTheList && !self.game.koIsRepeatingStageNow()) {
                    self.addCurrentWordToRepeatList();
                }
                return false;
            }
        }
        return true;
    };
    self.moveLetter = function (letter) {
        self.game.currentWord.koLetters.push(letter);

        var gameWords = self.game.koCurrentWords();
        var currentStep = self.game.koCurrentStep();
        var shuffledLetters = gameWords[currentStep].koShuffledLetters();
        var index = shuffledLetters.indexOf(letter);

        gameWords[currentStep].koShuffledLetters.splice(index, 1);

        if (shuffledLetters.length == 0) {
            self.wordWasChecked();
        }
    };
    self.wordWasChecked = function () {
        var gameWords = self.game.koCurrentWords();
        var currentStep = self.game.koCurrentStep();

        self.game.currentWord
            .koTranslation(gameWords[currentStep].translation);

        self.game.koIsStepFinished(true);
    };
    self.hasMoreWords = function (includeWordsNeedRepeat) {
        var currentStep = self.game.koCurrentStep();
        var wordsCount = self.game.koCurrentWords().length;
        var wordsNeedRepeatCount = self.game.koWordsNeedRepeat().length;
        var hasWords = false;
        if (includeWordsNeedRepeat) {
            hasWords = (self.game.koIsRepeatingStageNow() && (currentStep + 1) < wordsCount) ||
                (!self.game.koIsRepeatingStageNow() && ((currentStep + 1) < wordsCount ||
                    wordsNeedRepeatCount > 0));
        }
        else {
            hasWords = (currentStep + 1) < wordsCount;
        }
        return hasWords;
    };
    self.resetCurrentWord = function () {
        var currentWord = self.game.currentWord;
        currentWord.koId(0);
        currentWord.koLetters([]);
        currentWord.koTranslation('');
    };
    self.repeatWords = function () {
        self.resetCurrentWord();
        self.game.koIsRepeatingStageNow(true);
        self.game.koCurrentWords(self.game.koWordsNeedRepeat());
        self.game.koCurrentStep(0);

        var firstWord = self.game.koCurrentWords()[0];
        self.setCurrentWord(firstWord.wordId, firstWord.translation);
    }

    return self;
}