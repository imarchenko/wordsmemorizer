﻿var wordsMemorizer_progressHelper = {
    index: 0,
    step: 1,
    minValue: 0,
    maxValue: 100,
    progressBarElements: [],
    
    init: function () {
        var progressBars = document.querySelectorAll(".form-step-by-step");
        wordsMemorizer_progressHelper.progressBarElements = Array.prototype.slice.call(progressBars);

        wordsMemorizer_progressHelper.setStep(wordsMemorizer_progressHelper.index);
    },
    hideStep: function(){
        wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
            var currentElement = item.querySelector("[data-step='" + wordsMemorizer_progressHelper.index + "']");
            if (currentElement != undefined) {
                $(currentElement).hide("slow");
            }
        });
    },
    next: function () {
        if (wordsMemorizer_progressHelper.index <= wordsMemorizer_progressHelper.maxValue) {
            var nextStep = wordsMemorizer_progressHelper.index + wordsMemorizer_progressHelper.step;
            wordsMemorizer_progressHelper.setStep(nextStep);
        }
        if (wordsMemorizer_progressHelper.index == wordsMemorizer_progressHelper.maxValue - 1) {
            wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
                var nextBtn = item.querySelector(".next-step");
                $(nextBtn).hide();
                var lastBtn = item.querySelector(".last-step");
                $(lastBtn).show("slow");
            });
        }
        else if (wordsMemorizer_progressHelper.index == wordsMemorizer_progressHelper.maxValue) {
            wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
                var nextBtn = item.querySelector(".next-step");
                $(nextBtn).hide();
                var lastBtn = item.querySelector(".last-step");
                $(lastBtn).hide();
                var backBtn = item.querySelector(".back-step");
                $(backBtn).hide();
            });
        }
        if (wordsMemorizer_progressHelper.index > wordsMemorizer_progressHelper.minValue
                && wordsMemorizer_progressHelper.index < wordsMemorizer_progressHelper.maxValue) {
            wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
                var backBtn = item.querySelector(".back-step");
                $(backBtn).show();
            });
        }
    },
    setStep: function(index){
        wordsMemorizer_progressHelper.hideStep();

        wordsMemorizer_progressHelper.index = index;

        wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
            var currentElement = item.querySelector("[data-step='" + index + "']");
            if (currentElement != undefined) {
                $(currentElement).show("fast", function () {
                    $(currentElement).find("input").focus();
                });
            }
            var progressBar = item.querySelector(".progress-bar");
            progressBar.setAttribute("aria-valuenow", index);
            progressBar.style.width = (index * 100 / wordsMemorizer_progressHelper.maxValue) + "%";
            var textElement = progressBar.querySelector(".text");
            textElement.textContent = index + "/" + wordsMemorizer_progressHelper.maxValue;
        });
    },
    back: function () {
        if (wordsMemorizer_progressHelper.index == wordsMemorizer_progressHelper.maxValue) {
            wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
                var lastBtn = item.querySelector(".last-step");
                $(lastBtn).hide();
                var nextBtn = item.querySelector(".next-step");
                $(nextBtn).show("slow");
            });
        }
        if (wordsMemorizer_progressHelper.index > wordsMemorizer_progressHelper.minValue) {
            var nextStep = wordsMemorizer_progressHelper.index - wordsMemorizer_progressHelper.step;
            wordsMemorizer_progressHelper.setStep(nextStep);
        }
        if (wordsMemorizer_progressHelper.index == wordsMemorizer_progressHelper.minValue) {
            wordsMemorizer_progressHelper.progressBarElements.forEach(function (item, i) {
                var backBtn = item.querySelector(".back-step");
                $(backBtn).hide();
            });
        }
    },
    reset: function () {
        wordsMemorizer_progressHelper.hideStep();
        wordsMemorizer_progressHelper.index = wordsMemorizer_progressHelper.minValue;
        wordsMemorizer_progressHelper.init();
    },
    isSaveStep: function () {
        return wordsMemorizer_progressHelper.index == wordsMemorizer_progressHelper.maxValue - 1;
    }
}