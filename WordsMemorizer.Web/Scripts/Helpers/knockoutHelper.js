﻿var wordsMemorizer_knockoutHelper = {
    getContentViewModel: {
        contentData: ko.observable(),

        isContentEmpty: function () {
            var self = wordsMemorizer_knockoutHelper.getContentViewModel;
            return !self.loading() && self.contentData() != undefined && self.contentData().length == 0;
        },

        loading: ko.observable(true),

        pageViewModel: ko.observable()
    },

    applyBindings: function (data) {
        wordsMemorizer_knockoutHelper.getContentViewModel.contentData(data);

        ko.applyBindings(wordsMemorizer_knockoutHelper.getContentViewModel);
    },

    getContent: function (url, data, method, callback) {
        if (method == undefined) {
            method = "post";
        }
        $.ajax({
            url: url,
            method: method,
            data: data,
            dataType: "json",
            success: function (response) {
                wordsMemorizer_knockoutHelper.getContentViewModel.loading(false);
                if (response != null) {
                    wordsMemorizer_knockoutHelper.getContentViewModel.contentData(response.data);
                    $(".onBind-fadeIn").fadeIn("slow");

                    if (callback != undefined)
                    {
                        callback();
                    }
                }
            }
        });
    }
}