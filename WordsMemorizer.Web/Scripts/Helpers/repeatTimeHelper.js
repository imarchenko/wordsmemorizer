﻿var wordsMemorizer_repeatTimeHelper = {
    getTimeText: function(scheduleObj) {
        var currentDateTime = new Date();
        if (scheduleObj.lastRepeat == undefined) {
            return "Не тренировалось";
        }
        var lastRepeatDate = new Date(scheduleObj.lastRepeat);
        var repeatDate = lastRepeatDate.addHours(scheduleObj.hoursToRepeat);
        var diff = repeatDate.getDiffInMin(currentDateTime);
        if (diff < 1) {
            return "Доступно";
        }
        if (diff < 60) {
            return Math.round(diff) + " минут";
        }
        diff /= 60;
        if(diff < 24) {
            return Math.round(diff) + " часов";
        }
        diff /= 24;
        if (diff < 30) {
            return Math.round(diff) + " дней";
        }
        diff /= 30;
        if (diff < 12) {
            return Math.round(diff) + " месяцев";
        }
        diff /= 12;
        return Math.round(diff) + " лет";
    }
};