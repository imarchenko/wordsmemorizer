﻿var wordsMemorizer_ajaxFormHelper = {
    selector: ".form-ajax",
    submitButtomSelector: ".ajaxButton",
    $progress: null,

    showProgress: function showProgress() {
        $progress = $(wordsMemorizer_ajaxFormHelper.selector).find(".ajaxProgress:first");
        $progress.show();

        $(wordsMemorizer_ajaxFormHelper.submitButtomSelector).enable(false);
    },
    hideProgress: function hideProgress() {
        $progress.hide();

        $(wordsMemorizer_ajaxFormHelper.submitButtomSelector).enable(true);
    },
    showSuccessIcon: function showSuccessIcon() {
        $progress.find("img").attr("src", "/Images/check.png");
        $progress.show();
    },

    bindAll: function(){
        var formOptions = {
            beforeSubmit: wordsMemorizer_ajaxFormHelper.showProgress,  // pre-submit callback
            success: function (apiResult) {
                wordsMemorizer_ajaxFormHelper.hideProgress();

                if (apiResult.error == null) {
                    wordsMemorizer_ajaxFormHelper.showSuccessIcon();

                    setInterval(function () {
                        $(wordsMemorizer_ajaxFormHelper.selector).hide("drop", {}, 500, function () {
                            location.href = apiResult.data;
                        });
                    }, 1000);
                }
                else {
                    $(".summary:first").text(apiResult.error);
                }
            },
            error: function (result) {
                $(".summary:first").text(result);
            }
        };
        $(wordsMemorizer_ajaxFormHelper.selector).ajaxForm(formOptions);
    },
     
    manuallySubmit: function ($form, onSuccess) {
        var submitOptions = {
            beforeSubmit: wordsMemorizer_ajaxFormHelper.showProgress,  // pre-submit callback
            success: function (apiResult) {
                wordsMemorizer_ajaxFormHelper.hideProgress();

                if (apiResult.error == null) {
                    wordsMemorizer_ajaxFormHelper.showSuccessIcon();
                    onSuccess(apiResult);
                    wordsMemorizer_ajaxFormHelper.hideProgress();
                }
                else {
                    $(".summary:first").text(apiResult.error);
                }
            },
            error: function (result) {
                $(".summary:first").text(result);
            }
        };
        $form.ajaxSubmit(submitOptions);

        return false;
    }
}