﻿using System;
using System.Web.Mvc;

namespace WordsMemorizer.Web.Extensions
{
    public static class TempDataDictionaryExtensions
    {
        public static void Add<T>(this TempDataDictionary tempDataDictionary, T value)
        {
            if (tempDataDictionary == null)
            {
                throw new ArgumentNullException("tempDataDictionary");
            }
            if (Equals(value, null))
            {
                throw new ArgumentNullException("value");
            }
            tempDataDictionary[value.GetType().FullName] = value;
        }

        public static bool TryGetValue<T>(this TempDataDictionary tempDataDictionary, out T value)
        {
            if (tempDataDictionary == null)
            {
                throw new ArgumentNullException("tempDataDictionary");
            }
            object tempDataValue;
            tempDataDictionary.TryGetValue(typeof(T).FullName, out tempDataValue);
            if (tempDataValue is T)
            {
                value = (T) tempDataValue;
                return true;
            }
            value = default(T);
            return false;
        }
    }
}