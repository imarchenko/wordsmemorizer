﻿using System;
using System.Web.Mvc;
using WordsMemorizer.Web.Filter;

namespace WordsMemorizer.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static void SaveModelStateInTempData(this Controller controller)
        {
            if (controller == null)
            {
                throw new ArgumentNullException("controller");
            }
            var modelState = controller.ViewData.ModelState;
            controller.TempData[RestoreModelStateAttribute.TempDataModelStateKey] = modelState;
        }

        public static string DisplayError(this Controller controller, string key, string errorMessage, string redirectUrl)
        {
            if (controller == null)
            {
                throw new ArgumentNullException("controller");
            }
            controller.ModelState.AddModelError(key, errorMessage);
            SaveModelStateInTempData(controller);
            return redirectUrl;
        }
    }
}