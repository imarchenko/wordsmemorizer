﻿using System;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace WordsMemorizer.Web
{
    public class JsonNetResult : ActionResult
    {
        public static JsonSerializerSettings DefaultSerializerSettings { get; set; }

        public static Formatting DefaultFormatting { get; set; }

        public Encoding ContentEncoding { get; set; }

        public string ContentType { get; set; }

        public object Data { get; set; }

        public JsonSerializerSettings SerializerSettings { get; set; }

        public Formatting Formatting { get; set; }

        static JsonNetResult()
        {
            DefaultSerializerSettings = new JsonSerializerSettings();
            DefaultFormatting = Formatting.None;
        }

        public JsonNetResult()
        {
            SerializerSettings = DefaultSerializerSettings;
            Formatting = DefaultFormatting;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var response = context.HttpContext.Response;

            response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data == null)
            {
                return;
            }
            var writer = new JsonTextWriter(response.Output)
            {
                Formatting = Formatting
            };

            var serializer = JsonSerializer.Create(SerializerSettings);
            serializer.Serialize(writer, Data);

            writer.Flush();
        }
    }
}