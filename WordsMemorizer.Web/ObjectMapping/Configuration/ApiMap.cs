﻿using AutoMapper;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Web.ViewModels.Api.Dictionary;

namespace WordsMemorizer.Web.ObjectMapping.Configuration
{
    public class ApiMap : Profile
    {
        protected override void Configure()
        {
            // languages
            Mapper.CreateMap<DictionaryApiAddLanguageForm, UserLanguage>();

            Mapper.CreateMap<UserLanguage, DictionaryApiEditLanguageForm>();

            Mapper.CreateMap<DictionaryApiEditLanguageForm, UserLanguage>();

            // words
            Mapper.CreateMap<DictionaryApiBaseWordForm, UserWord>()
                .ForMember(d => d.ForeignWord, c => c.Ignore())
                .ForMember(d => d.Transcription, c => c.Ignore())
                .ForMember(d => d.Word, c => c.Ignore());
        }
    }
}