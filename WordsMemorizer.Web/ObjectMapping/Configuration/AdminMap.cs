﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Areas.Admin.ViewModels.Users;

namespace WordsMemorizer.Web.ObjectMapping.Configuration
{
    public class AdminMap : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<List<UserAccount>, AdminUsersViewModel>()
                .ForMember(d => d.Form, c => c.Ignore())
                .ForMember(d => d.Users, c => c.MapFrom(s => s));

            Mapper.CreateMap<UserAccount, AdminUsersViewModel.User>()
                .ForMember(d => d.FullName, c => c.MapFrom(s => s.Profile.FullName))
                .ForMember(d => d.RegistrationDate, c => c.MapFrom(s => s.Profile.RegistrationDate));
        }
    }
}