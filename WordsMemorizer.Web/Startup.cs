﻿using System.Configuration;
using Hangfire;
using Microsoft.Owin;
using Owin;
using WordsMemorizer.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace WordsMemorizer.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureApp(app);

            GlobalConfiguration.Configuration
                .UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
