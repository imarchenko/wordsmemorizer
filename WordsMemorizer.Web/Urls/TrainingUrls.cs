﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WordsMemorizer.Web.Urls
{
    public static class TrainingUrls
    {
        public static string TrainingIndex(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Index", "Training", new { area = "" });
        }

        public static string TrainingWordsConstructor(this UrlHelper urlHelper, int? groupId)
        {
            return urlHelper.Action("WordsConstructor", "Training", new { area = "", groupId });
        }
    }
}