﻿using System.Web.Mvc;

namespace WordsMemorizer.Web.Urls
{
    public static class HomeUrls
    {
        public static string HomeIndex(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Index", "Home", new { area = "" });
        }
    }
}