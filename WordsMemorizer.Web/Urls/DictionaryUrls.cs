﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WordsMemorizer.Web.Urls
{
    public static class DictionaryUrls
    {
        public static string DictionaryAddLanguage(this UrlHelper urlHelper)
        {
            return urlHelper.Action("AddLanguage", "Dictionary", new { area = "" });
        }

        public static string DictionaryEditLanguage(this UrlHelper urlHelper, int? id = null)
        {
            return urlHelper.Action("EditLanguage", "Dictionary", new { area = "", id });
        }

        public static string DictionaryIndex(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Index", "Dictionary", new { area = "" });
        }

        public static string DictionaryViewWords(this UrlHelper urlHelper, int? languageId = null)
        {
            return urlHelper.Action("ViewWords", "Dictionary", new { area = "", languageId });
        }

        public static string DictionaryAddWord(this UrlHelper urlHelper, int? languageId = null)
        {
            return urlHelper.Action("AddWord", "Dictionary", new { area = "", languageId });
        }
        
        public static string DictionaryEditWord(this UrlHelper urlHelper)
        {
            return urlHelper.Action("EditWord", "Dictionary", new { area = "" });
        }
    }
}