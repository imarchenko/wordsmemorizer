﻿using System.Web.Mvc;

namespace WordsMemorizer.Web.Urls
{
    public static class AccountUrls
    {
        public static string AccountRegister(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Register", "Account", new { area = "" });
        }

        public static string AccountLogin(this UrlHelper urlHelper, string returnUrl = null)
        {
            return urlHelper.Action("Login", "Account", new { area = "", returnUrl });
        }

        public static string AccountChangePassword(this UrlHelper urlHelper)
        {
            return urlHelper.Action("ChangePassword", "Account", new { area = "" });
        }

        public static string AccountConfirmEmail(this UrlHelper urlHelper, int userId, string code)
        {
            return urlHelper.Action("ConfirmEmail", "Account", new { area = "", userId, code });
        }

        public static string AccountSendConfirmation(this UrlHelper urlHelper)
        {
            return urlHelper.Action("SendConfirmation", "Account", new { area = "" });
        }

        public static string AccountRelogin(this UrlHelper urlHelper, string returnUrl = null)
        {
            return urlHelper.Action("Relogin", "Account", new { area = "", returnUrl });
        }

        public static string AccountForgotPassword(this UrlHelper urlHelper)
        {
            return urlHelper.Action("ForgotPassword", "Account", new { area = "" });
        }
    }
}