﻿using System.Web.Mvc;

namespace WordsMemorizer.Web.Urls
{
    public static class ApiUrls
    {
        public static string AccountApiRegister(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Register", "AccountApi", new { area = "" });
        }

        public static string AccountApiLogin(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Login", "AccountApi", new { area = "" });
        }

        public static string DictionaryApiGetLanguages(this UrlHelper urlHelper)
        {
            return urlHelper.Action("GetLanguages", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiGetLanguage(this UrlHelper urlHelper)
        {
            return urlHelper.Action("GetLanguage", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiAddLanguage(this UrlHelper urlHelper)
        {
            return urlHelper.Action("AddLanguage", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiEditLanguage(this UrlHelper urlHelper)
        {
            return urlHelper.Action("EditLanguage", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiDeleteLanguage(this UrlHelper urlHelper)
        {
            return urlHelper.Action("DeleteLanguage", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiGetWords(this UrlHelper urlHelper)
        {
            return urlHelper.Action("GetWords", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiGetWord(this UrlHelper urlHelper)
        {
            return urlHelper.Action("GetWord", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiAddWord(this UrlHelper urlHelper)
        {
            return urlHelper.Action("AddWord", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiEditWord(this UrlHelper urlHelper)
        {
            return urlHelper.Action("EditWord", "DictionaryApi", new { area = "" });
        }

        public static string DictionaryApiDeleteWord(this UrlHelper urlHelper)
        {
            return urlHelper.Action("DeleteWord", "DictionaryApi", new { area = "" });
        }

        public static string TrainingApiGetWords(this UrlHelper urlHelper, int groupId)
        {
            return urlHelper.Action("GetWords", "TrainingApi", new { area = "", groupId });
        }

        public static string TrainingApiSaveTraining(this UrlHelper urlHelper)
        {
            return urlHelper.Action("SaveTraining", "TrainingApi", new { area = "" });
        }
    }
}