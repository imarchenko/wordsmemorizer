﻿using System.Web.Mvc;

namespace WordsMemorizer.Web.Urls
{
    public static class AdminUrls
    {
        public static string AdminHomeIndex(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Index", "AdminHome", new { area = "Admin" });
        }

        public static string AdminHomeMain(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Main", "AdminHome", new { area = "Admin" });
        }

        public static string AdminUsersShow(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Show", "AdminUsers", new { area = "Admin" });
        }

        public static string AdminUsersBan(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Ban", "AdminUsers", new { area = "Admin" });
        }
        public static string AdminUsersUnban(this UrlHelper urlHelper)
        {
            return urlHelper.Action("Unban", "AdminUsers", new { area = "Admin" });
        }
    }
}