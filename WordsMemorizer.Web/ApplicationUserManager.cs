using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model.Entities;

namespace WordsMemorizer.Web
{
    public class ApplicationUserManager : UserManager<UserAccount, int>
    {
        public ApplicationUserManager(IUserStore<UserAccount, int> store) : base(store)
        {
        }

        /// <summary>
        /// Remove all old user roles and add a new one
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="roleName">adding role</param>
        public virtual async Task<IdentityResult> SetRoleAsync(int userId, string roleName)
        {
            var userRoleStore = (IUserRoleStore<UserAccount, int>)Store;

            var user = await FindByIdAsync(userId);
            if (user == null)
            {
                throw new InvalidOperationException("Invalid user Id");
            }

            // Remove user to each role using UserRoleStore
            var roles = await userRoleStore.GetRolesAsync(user);
            foreach (var role in roles)
            {
                await userRoleStore.RemoveFromRoleAsync(user, role);
            }
            await userRoleStore.AddToRoleAsync(user, roleName);

            // Call update once when all roles are removed
            return await UpdateAsync(user);
        }
    }
}
