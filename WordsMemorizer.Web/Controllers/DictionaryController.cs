﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WordsMemorizer.Model;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Web.Extensions;
using WordsMemorizer.Web.Filter;
using WordsMemorizer.Web.Urls;
using WordsMemorizer.Web.ViewModels.Dictionary;

namespace WordsMemorizer.Web.Controllers
{
    [Authorize]
    public class DictionaryController : Controller
    {
        private readonly MainDbContext _dbContext;

        private readonly ISettingService _settingsService;

        public DictionaryController(MainDbContext dbContext,
            ISettingService settingsService)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            if (settingsService == null)
            {
                throw new ArgumentNullException("settingsService");
            }
            _dbContext = dbContext;
            _settingsService = settingsService;
        }

        [RestoreModelState]
        public async Task<ActionResult> AddLanguage()
        {
            var model = new DictionaryAddLanguageViewModel();

            return View(model);
        }

        [RestoreModelState]
        public async Task<ActionResult> EditLanguage(int? id = null)
        {
            if (!id.HasValue)
            {
                return new HttpNotFoundResult();
            }
            var model = new DictionaryEditLanguageViewModel();
            model.EditLanguageForm.Id = id.Value;

            return View(model);
        }

        public async Task<ActionResult> Index()
        {
            var model = new DictionaryIndexViewModel();
            return View(model);
        }

        [RestoreModelState]
        public ActionResult ViewWords(int? languageId = null)
        {
            if (!languageId.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var model = new DictionaryViewWordsViewModel();
            model.LanguageId = languageId.Value;
            return View(model);
        }

        [RestoreModelState]
        public ActionResult AddWord(int? languageId = null)
        {
            if (!languageId.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var model = new DictionaryAddWordViewModel();
            model.AddWordForm.LanguageId = languageId.Value;
            return View(model);
        }

        [RestoreModelState]
        public ActionResult EditWord(int? languageId = null, int? wordId = null)
        {
            if (!wordId.HasValue || !languageId.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var model = new DictionaryEditWordViewModel();
            model.EditWordForm.WordId = wordId;
            model.LanguageId = languageId.Value;
            return View(model);
        }
    }
}