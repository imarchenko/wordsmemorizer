﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WordsMemorizer.Model;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Model.SettingEntities;
using WordsMemorizer.Model.ValueObjects;
using WordsMemorizer.ViewModels.Account;
using WordsMemorizer.Web;
using WordsMemorizer.Web.Extensions;
using WordsMemorizer.Web.Filter;
using WordsMemorizer.Web.Urls;

namespace WordsMemorizer.Controllers
{
    public class AccountController : Controller
    {
        private readonly MainDbContext _dbContext;

        private readonly ISettingService _settingsService;

        public AccountController(MainDbContext dbContext,
            ISettingService settingsService)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            if (settingsService == null)
            {
                throw new ArgumentNullException("settingsService");
            }
            _dbContext = dbContext;
            _settingsService = settingsService;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                var manager = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
                if (manager.EmailService == null)
                {
                    var host = ConfigurationManager.AppSettings["SmtpHost"];
                    var login = ConfigurationManager.AppSettings["SmtpLogin"];
                    var pass = ConfigurationManager.AppSettings["SmtpPassword"];
                    var port = 25;
                    var portStr = ConfigurationManager.AppSettings["SmtpPort"];
                    Int32.TryParse(portStr, out port);
                    manager.EmailService = new EmailService(host, port, login, pass);
                }
                return manager;
            }
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [RestoreModelState]
        public async Task<ActionResult> Login(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.HomeIndex();

            var model = new AccountLoginViewModel();
            model.LoginForm.ReturnUrl = returnUrl;

            return View(model);
        }

        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return Redirect(Url.HomeIndex());
        }

        [RestoreModelState]
        public async Task<ActionResult> Register()
        {
            var model = new AccountRegisterViewModel();
            return View(model);
        }

        [Authorize]
        public async Task<ActionResult> Relogin(string returnUrl)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            AuthenticationManager.SignOut();
            return SignInAsync(user, true, returnUrl);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect(Url.HomeIndex());
        }

        private ActionResult SignInAsync(UserAccount user, bool isPersistent, string returnUrl = null)
        {
            var mainSettings = _settingsService.GetSettingEntry<MainSettings>();
            var emailService = UserManager.EmailService as EmailService;
            if (false && emailService.IsAvailable())
            {
                TempData["ConfirmationState"] = EmailConfirmationState.NotSent;
                return RedirectToLocal(Url.AccountSendConfirmation());
            }
            if (user.State == UserAccountState.Blocked)
            {
                var msg = "Данный пользователь заблокирован. ";
                if (!String.IsNullOrEmpty(mainSettings.EmailSupport))
                {
                    msg = String.Format("Вы можете также задавать вопросы отослав письмо на {0}", mainSettings.EmailSupport);
                }
                TempData["Error"] = msg;
                //ModelState.AddModelError("", msg);
                return Redirect(Url.AccountLogin());
            }
            if (returnUrl == null)
            {
                //returnUrl = Url.ProfileViewMyProfile();
                returnUrl = Url.HomeIndex();
            }
            IdentityHelper.SignIn(UserManager, user, isPersistent);
            return RedirectToLocal(returnUrl);
        }

        //[HttpPost]
        //public async Task<ActionResult> Register(AccountRegisterForm form)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        this.SaveModelStateInTempData();
        //        return Redirect(Url.AccountRegister());
        //    }
        //    var user = new UserAccount
        //    {
        //        UserName = form.UserName,
        //        Email = form.Email,
        //        Profile = new UserProfile
        //        {
        //            RegistrationDate = DateTime.Now
        //        }
        //    };
        //    using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
        //    {
        //        var userCreateResult = await UserManager.CreateAsync(user, form.Password);
        //        if (!userCreateResult.Succeeded)
        //        {
        //            AddErrors(userCreateResult);
        //            return Redirect(Url.AccountRegister());
        //        }
        //        _dbContext.SaveChanges();
    }
}