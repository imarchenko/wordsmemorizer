﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Web.ViewModels.Training;

namespace WordsMemorizer.Web.Controllers
{
    [Authorize]
    public class TrainingController : Controller
    {
        private readonly MainDbContext _dbContext;

        private readonly ISettingService _settingsService;

        public TrainingController(MainDbContext dbContext,
            ISettingService settingsService)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            if (settingsService == null)
            {
                throw new ArgumentNullException("settingsService");
            }
            _dbContext = dbContext;
            _settingsService = settingsService;
        }

        public async Task<ActionResult> Index()
        {
            var model = new TrainingIndexViewModel();

            var userId = User.Identity.GetUserId<int>();
            var groups = await _dbContext.TrainingGroups
                .Where(group => group.UserId == userId)
                .Include(group => group.Category)
                //.GroupBy(group => group.CategoryId)
                .ToListAsync();

            model.TrainingGroups.AddRange(groups.Select(group =>
            new SelectListItem
            {
                Text = group.RecordId.ToString(),
                Value = group.Id.ToString(),
                Group = new SelectListGroup
                {
                    Name = group.Category.Name
                }
            }));

            return View(model);
        }

        public ActionResult WordsConstructor(int? groupId)
        {
            if (!groupId.HasValue)
            {
                groupId = 0;
            }

            var model = new TrainingWordsConstructorViewModel();
            model.GroupId = groupId.Value;
            return View(model);
        }
    }
}