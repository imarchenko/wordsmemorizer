﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Hangfire;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model;
using WordsMemorizer.Web.CronConfig;
using WordsMemorizer.Web.ViewModels.Api;
using WordsMemorizer.Web.ViewModels.Api.Training;

namespace WordsMemorizer.Web.Controllers.Api
{
    [Authorize]
    public class TrainingApiController : Controller
    {
        private readonly IMainDbContext _dbContext;

        public TrainingApiController(IMainDbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<ActionResult> GetWords(int? groupID)
        {
            try
            {
                if (!groupID.HasValue)
                {
                    return null;
                }
                var apiResult = new ApiResult();
                var userId = User.Identity.GetUserId<int>();
                var words = await _dbContext.UserWords
                    .Include(i => i.Word.Translations)
                    .Include(i => i.Word.Pronounciations)
                    .Where(userWord => userWord.UserLanguageId == groupID.Value)
                    .Select(userWord => new TrainingApiWordViewModel
                    {
                        WordId = userWord.Id,
                        WordText = userWord.Word.Text,
                        Translations = userWord.UserTranslations.Select(userTranslation =>
                        new TrainingApiTranslationViewModel
                        {
                            Id = userTranslation.Id,
                            Text = (userTranslation.Translation.Word1.LagnuageId == userWord.UserLanguage.LanguageId
                                ? userTranslation.Translation.Word1.Text
                                : userTranslation.Translation.Word2.Text),
                            Popularity = userTranslation.Popularity,
                            Raiting = userTranslation.Raiting.Avarage,
                            VotesCount = userTranslation.Raiting.VotesCount,
                            HasMyVote = userTranslation.Raiting.Voutes
                                .Any(vote => vote.UserId == userId)
                        })
                    })
                    .ToListAsync();
                apiResult.Data = words;

                return new JsonNetResult
                {
                    Data = apiResult
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveTraining(int? groupID)
        {
            var words = await _dbContext.UserWords
                .Include(i => i.RepeatSchedule)
                .Where(word => word.LanguageId == groupID.Value)
                .ToListAsync();

            foreach (var word in words)
            {
                word.RepeatSchedule.Update(true);
                BackgroundJob.Schedule(() => 
                    CronJobs.WordIsReadyForTraining(word.Id),
                    TimeSpan.FromHours(word.RepeatSchedule.HoursToRepeat));
            }
            _dbContext.SaveChanges();

            return new JsonResult { Data = new ApiResult() };
        }
    }
}