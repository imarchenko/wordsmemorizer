﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Hangfire;
using Microsoft.AspNet.Identity;
using WordsMemorizer.Model;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Web.CronConfig;
using WordsMemorizer.Web.Urls;
using WordsMemorizer.Web.ViewModels.Api;
using WordsMemorizer.Web.ViewModels.Api.Dictionary;

namespace WordsMemorizer.Web.Controllers.Api
{
    [Authorize]
    public class DictionaryApiController : Controller
    {
        private readonly IMainDbContext _dbContext;

        private readonly ISettingService _settingsService;

        public DictionaryApiController(IMainDbContext dbContext,
            ISettingService settingsService)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            if (settingsService == null)
            {
                throw new ArgumentNullException("settingsService");
            }
            _dbContext = dbContext;
            _settingsService = settingsService;
        }

        [HttpPost]
        public async Task<ActionResult> GetLanguages()
        {
            var apiResult = new ApiResult();
            try
            {
                var userId = this.User.Identity.GetUserId<int>();
                var languages = await _dbContext.DictionaryLanguages
                    .Where(lang => lang.UserAccountId == userId)
                    .Select(lang => new DictionaryApiLanguageViewModel
                    {
                        Id = lang.Id,
                        Name = lang.Name,
                        WordsCount = _dbContext.UserWords.Count(word => word.LanguageId == lang.Id)
                    })
                    .ToListAsync();

                apiResult.Data = languages;
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetLanguage(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return null;
                }
                var language = await _dbContext.DictionaryLanguages
                    .Select(lang => new DictionaryApiLanguageViewModel
                    {
                        Id = lang.Id,
                        Name = lang.Name
                    })
                    .FirstOrDefaultAsync(lang => lang.Id == id);

                return new JsonNetResult
                {
                    Data = language
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddLanguage(DictionaryApiAddLanguageForm addLanguageForm)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!ModelState.IsValid)
                {
                    apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var userId = this.User.Identity.GetUserId<int>();

                var language = new UserLanguage();
                language.UserAccountId = userId;
                Mapper.Map(addLanguageForm, language);

                _dbContext.DictionaryLanguages.Add(language);
                await _dbContext.SaveChangesAsync();

                apiResult.Data = Url.DictionaryIndex();
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;//ApiResult.GeneralErrorMessage;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> EditLanguage(DictionaryApiEditLanguageForm editLanguageForm)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!ModelState.IsValid)
                {
                    apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var language = await _dbContext.DictionaryLanguages
                    .FirstOrDefaultAsync(lang => lang.Id == editLanguageForm.Id);
                Mapper.Map(editLanguageForm, language);

                await _dbContext.SaveChangesAsync();

                apiResult.Data = Url.DictionaryIndex();
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;//ApiResult.GeneralErrorMessage;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteLanguage(int? languageId = null)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!languageId.HasValue)
                {
                    apiResult.Error = ApiResult.GeneralErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var language = await _dbContext.DictionaryLanguages
                    .FirstOrDefaultAsync(lang => lang.Id == languageId.Value);

                _dbContext.DictionaryLanguages.Remove(language);
                await _dbContext.SaveChangesAsync();

                apiResult.Data = languageId;
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;//ApiResult.GeneralErrorMessage;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetWords(int? languageId = null)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!languageId.HasValue)
                {
                    apiResult.Error = ApiResult.GeneralErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var words = await _dbContext.UserWords
                    .Include(userWord => userWord.RepeatSchedule)
                    .Where(userWord => userWord.UserLanguage.LanguageId == languageId.Value)
                    //.Select(userWord => new Tran)
                    .ToListAsync();

                apiResult.Data = words;
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetWord(int? wordId = null)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!wordId.HasValue)
                {
                    apiResult.Error = ApiResult.GeneralErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var word = await _dbContext.UserWords
                    .FirstOrDefaultAsync(_word => _word.Id == wordId.Value);

                apiResult.Data = word;
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddWord(DictionaryApiAddWordForm addWordForm)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!ModelState.IsValid)
                {
                    apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var word = new UserWord();
                word = await UpdateWord(addWordForm, word);

                _dbContext.UserWords.Add(word);
                await _dbContext.SaveChangesAsync();

                BackgroundJob.Schedule(() =>
                    CronJobs.WordIsReadyForTraining(word.Id),
                    TimeSpan.FromHours(word.RepeatSchedule.HoursToRepeat));

                apiResult.Data = Url.DictionaryViewWords(addWordForm.LanguageId);
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<ActionResult> EditWord(DictionaryApiEditWordForm editWordForm)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!ModelState.IsValid)
                {
                    apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                if (!editWordForm.WordId.HasValue)
                {
                    apiResult.Error = ApiResult.GeneralErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var word = await _dbContext.UserWords
                    .FirstOrDefaultAsync(_word => _word.Id == editWordForm.WordId.Value);

                if (word == null)
                {
                    apiResult.Error = ApiResult.GeneralErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }

                word = await UpdateWord(editWordForm, word);
                //Mapper.Map(editWordForm, word);
                await _dbContext.SaveChangesAsync();

                apiResult.Data = Url.DictionaryViewWords(word.LanguageId);
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;
                return new JsonNetResult { Data = apiResult };
            }
        }

        private async Task<UserWord> UpdateWord(DictionaryApiBaseWordForm wordForm, UserWord word)
        {
            Mapper.Map(wordForm, word);

            var foreignWord = await _dbContext.Words
                .FirstOrDefaultAsync(w => w.Id == wordForm.ForeignWordId);
            if (foreignWord == null)
            {
                foreignWord = new Word();
                foreignWord.Text = wordForm.Text;
            }
            var transcription = await _dbContext.WordPronunciations
                .FirstOrDefaultAsync(w => w.Id == wordForm.TranscriptionId);
            if (transcription == null)
            {
                transcription = new Pronounciation();
                transcription.Text = wordForm.Transcription;
            }
            var translation = await _dbContext.Words
                .FirstOrDefaultAsync(w => w.Id == wordForm.TranscriptionId);
            if (translation == null)
            {
                translation = new Word();
                translation.Text = wordForm.Translation;
            }

            foreignWord.Translations.Add(translation);
            transcription.ForeignWord = foreignWord;
            word.ForeignWord = foreignWord;
            word.Transcription = transcription;
            word.Word = translation;

            return word;
        }

        [HttpPost]
        public async Task<ActionResult> DeleteWord(int? wordId = null)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!ModelState.IsValid)
                {
                    apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                if (!wordId.HasValue)
                {
                    apiResult.Error = ApiResult.GeneralErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var word = await _dbContext.UserWords.FirstOrDefaultAsync(_word => _word.Id == wordId.Value);
                _dbContext.UserWords.Remove(word);
                await _dbContext.SaveChangesAsync();

                apiResult.Data = word.Id;
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;
                return new JsonNetResult { Data = apiResult };
            }
        }
    }
}