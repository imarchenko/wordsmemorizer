﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using WordsMemorizer.Model;
using WordsMemorizer.Model.Entities;
using WordsMemorizer.Model.Service;
using WordsMemorizer.Model.SettingEntities;
using WordsMemorizer.Model.ValueObjects;
using WordsMemorizer.Web.ViewModels.Api;
using WordsMemorizer.Web.ViewModels.Api.Account;
using WordsMemorizer.Web.Urls;

namespace WordsMemorizer.Web.Controllers.Api
{
    public class AccountApiController : Controller
    {
        private readonly IMainDbContext _dbContext;

        private readonly ISettingService _settingsService;

        public AccountApiController(IMainDbContext dbContext, ISettingService settingsService)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            if (settingsService == null)
            {
                throw new ArgumentNullException("settingsService");
            }
            _dbContext = dbContext;
            _settingsService = settingsService;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                var manager = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
                if (manager.EmailService == null)
                {
                    var host = ConfigurationManager.AppSettings["SmtpHost"];
                    var login = ConfigurationManager.AppSettings["SmtpLogin"];
                    var pass = ConfigurationManager.AppSettings["SmtpPassword"];
                    var port = 25;
                    var portStr = ConfigurationManager.AppSettings["SmtpPort"];
                    Int32.TryParse(portStr, out port);
                    manager.EmailService = new EmailService(host, port, login, pass);
                }
                return manager;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Login(AccountApiLoginForm loginForm)
        {
            var apiResult = new ApiResult();
            try
            {
                if (!ModelState.IsValid)
                {
                    apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                    return new JsonNetResult { Data = apiResult };
                }
                var user = await UserManager.FindByNameAsync(loginForm.UserName);
                if (user == null)
                {
                    user = await UserManager.FindByEmailAsync(loginForm.UserName);
                }
                if (user == null)
                {
                    apiResult.Error = "Пользователь с таким логином не найден";
                }
                else
                {
                    apiResult.Data = SignInAsync(user, true, loginForm.ReturnUrl);
                }
                return new JsonNetResult { Data = apiResult };
            }
            catch (Exception ex)
            {
                apiResult.Error = ex.Message;//ApiResult.GeneralErrorMessage;
                return new JsonNetResult { Data = apiResult };
            }
        }

        [HttpPost]
        public async Task<JsonNetResult> Register(AccountApiRegisterForm registerForm)
        {
            var apiResult = new ApiResult();
            try
	        {
		        if (!ModelState.IsValid)
                    {
                        apiResult.Error = ModelState.First().Value.Errors.First().ErrorMessage;
                        return new JsonNetResult { Data = apiResult };
                    }
                    var user = new UserAccount
                    {
                        UserName = registerForm.UserName,
                        Email = registerForm.Email,
                        Profile = new UserProfile
                        {
                            RegistrationDate = DateTime.Now
                        }
                    };
                    using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                    {
                        var userCreateResult = await UserManager.CreateAsync(user, registerForm.Password);
                        if (!userCreateResult.Succeeded)
                        {
                            apiResult.Error = userCreateResult.Errors.First();
                            return new JsonNetResult { Data = apiResult };
                        }
                        _dbContext.SaveChanges();

                        scope.Complete();
                    }
                    apiResult.Data = SignInAsync(user, true, Url.HomeIndex());
                    return new JsonNetResult { Data = apiResult };
	        }
	        catch (Exception ex)
	        {
                apiResult.Error = ex.Message; //ApiResult.GeneralErrorMessage;
                return new JsonNetResult { Data = apiResult };
	        }
        }

        /// <summary></summary>
        /// <param name="user"></param>
        /// <param name="isPersistent"></param>
        /// <param name="returnUrl"></param>
        /// <returns>Redirect url</returns>
        private string SignInAsync(UserAccount user, bool isPersistent, string returnUrl = null)
        {
            var mainSettings = _settingsService.GetSettingEntry<MainSettings>();
            var emailService = UserManager.EmailService as EmailService;
            if (false && emailService.IsAvailable())
            {
                TempData["ConfirmationState"] = EmailConfirmationState.NotSent;
                return Url.AccountSendConfirmation();
            }
            if (user.State == UserAccountState.Blocked)
            {
                var msg = "Данный пользователь заблокирован. ";
                if (!String.IsNullOrEmpty(mainSettings.EmailSupport))
                {
                    msg = String.Format("Вы можете также задавать вопросы отослав письмо на {0}", mainSettings.EmailSupport);
                }
                TempData["Error"] = msg;
                //ModelState.AddModelError("", msg);
                return Url.AccountLogin();
            }
            if (returnUrl == null)
            {
                //returnUrl = Url.ProfileViewMyProfile();
                returnUrl = Url.HomeIndex();
            }
            IdentityHelper.SignIn(UserManager, user, isPersistent);
            return returnUrl;
        }
    }
}