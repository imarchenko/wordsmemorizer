﻿using System.Web.Mvc;

namespace WordsMemorizer.Web.Filter
{
    public class RestoreModelStateAttribute : ActionFilterAttribute
    {
        public const string TempDataModelStateKey = "__ModelState";
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var tempData = filterContext.Controller.TempData;
            var modelState = filterContext.Controller.ViewData.ModelState;

            var savedModelState = tempData[TempDataModelStateKey];
            if (savedModelState != null && modelState.Equals(savedModelState) == false)
            {
                modelState.Merge((ModelStateDictionary)savedModelState);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}