﻿using System;
using System.Linq.Expressions;

namespace WordsMemorizer.Util
{
    public static class PropertyHelper
    {

        // <summary>
        // Get the name of a static or instance property from a property access lambda.
        // </summary>
        // <typeparam name="T">Type of the property</typeparam>
        // <param name="propertyLambda">lambda expression of the form: '() => Class.Property' or '() => object.Property'</param>
        // <returns>The name of the property</returns>
        public static string StringifyPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            return StringifyPropertyNameExpression(propertyLambda.Body);
        }

        // <summary>
        // Get the name of a static or instance property from a property access lambda.
        // </summary>
        // <typeparam name="T">Type of the property</typeparam>
        // <param name="propertyLambda">lambda expression of the form: '() => Class.Property' or '() => object.Property'</param>
        // <returns>The name of the property</returns>
        public static string StringifyMemberAccess<T>(Expression<Func<T>> propertyLambda)
        {
            return StringifyMemberAccessExpression(propertyLambda.Body);
        }

        // <summary>
        // Get the name of a static or instance property from a property access lambda.
        // </summary>
        // <typeparam name="T">Type of the property</typeparam>
        // <param name="propertyLambda">lambda expression of the form: '() => Class.Property' or '() => object.Property'</param>
        // <returns>The name of the property</returns>
        public static string StringifyMemberAccess<TObject, TResult>(Expression<Func<TObject, TResult>> propertyLambda)
        {
            return StringifyMemberAccessExpression(propertyLambda.Body);
        }

        public static string StringifyPropertyNameExpression(Expression expression)
        {
            var memberExpression = expression as MemberExpression;
            if (memberExpression != null)
            {
                return memberExpression.Member.Name;
            }
            throw new NotSupportedException();
        }



        public static string StringifyMemberAccessExpression(Expression expression)
        {
            var memberExpression = expression as MemberExpression;
            if (memberExpression != null)
            {
                if (memberExpression.Expression.NodeType ==
                    ExpressionType.MemberAccess)
                    return StringifyMemberAccessExpression(memberExpression.Expression) + "." + memberExpression.Member.Name;
                return memberExpression.Member.Name;
            }
            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                if (unaryExpression.NodeType != ExpressionType.Convert)
                    throw new Exception(string.Format("Cannot interpret member from {0}", unaryExpression));
                return StringifyMemberAccessExpression(unaryExpression.Operand);
            }
            throw new Exception(string.Format("Could not determine member from {0}", expression));
        }
    }
}