﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace WordsMemorizer.Util
{
    public static class MailBox
    {
        public static async Task<bool> SendEmails(string from, string subject, string body, IEnumerable<Tuple<string, Guid>> recipients, string login, string password, int port, string host, string unsubscribeUrl)
        {
            if (String.IsNullOrEmpty(login) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(host) || recipients == null)
            {
                return false;
            }
            var mailMessage = new MailMessage
            {
                From = new MailAddress(from),
                Subject = subject,
                IsBodyHtml = true,
                BodyEncoding = Encoding.UTF8
            };
            var smtpServer = new SmtpClient(host)
            {
                Port = port,
                EnableSsl = true,
                Credentials = new NetworkCredential(login, password)
            };
            foreach (var recipient in recipients)
            {
                mailMessage.To.Add(recipient.Item1); // add email
                mailMessage.Body = String.IsNullOrEmpty(unsubscribeUrl) 
                    ? body
                    : string.Format("{0}<br/><hr/><a href='{1}?key={2}'>Unsubscribe</a>", body, unsubscribeUrl, recipient.Item2);
            }
            if (mailMessage.To.Count > 0)
            {
                await smtpServer.SendMailAsync(mailMessage); 
            }

            smtpServer.Dispose();
            mailMessage.Dispose();
            return true;
        }
    }
}
