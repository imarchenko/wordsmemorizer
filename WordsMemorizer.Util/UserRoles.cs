﻿namespace WordsMemorizer.Util
{
    public class UserRoles
    {
        public const string All = "*";
        public const string User = "User";
        public const string Admin = "Admin";

        public const string RolesWithAccessToAdminPanel = Admin;
    }
}
